package pl.lodz.sda.mapper;

import org.apache.commons.csv.CSVRecord;
import pl.lodz.sda.dao.Employee;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class EmployeeMapper {
    public static List<Employee> mapCSVRecordToEmployee(List<CSVRecord> list){
        List<Employee> finallist = new ArrayList<>();

        list.forEach(row -> finallist.add(toEmployee(row)));
        return finallist;
    }

    public static Employee toEmployee(CSVRecord row){
        return new Employee(Integer.parseInt(row.get("id")),
                Date.valueOf(row.get("birth_date")),
                Date.valueOf(row.get("hire_date")),
                row.get("first_name"),
                row.get("last_name"),
                row.get("gender").charAt(0));
    }
    public static List<String[]> mapEmployeeToCSVRecord(List<Employee> list){
        List<String[]> newList = new ArrayList<>();
        list.forEach(el -> {
            newList.add(new String[]{Integer.toString(el.getId()),
                    el.getBirth_date().toString(),
                    el.getFirst_name(),
                    el.getLast_name(),
                    String.valueOf(el.getGender()),
                    el.getHire_date().toString()});
        });
        return newList;
    }
}
