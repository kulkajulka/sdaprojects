package pl.lodz.sda;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Paths;
import java.util.List;

public class CSVWriter {

    private static String
            PATH = "/media/julia/hdd/SDAProjects/jdbc-connector/src/main/resources";
    public static void writeCSV(String fileName, CSVFormat csvFormat,List<String[]> list){
        try (Writer writer = new FileWriter(Paths.get(PATH, fileName).toFile())) {
            try(CSVPrinter printer = new CSVPrinter(writer,csvFormat)){
                list.forEach(el ->{
                    try {
                        printer.printRecord(el);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
