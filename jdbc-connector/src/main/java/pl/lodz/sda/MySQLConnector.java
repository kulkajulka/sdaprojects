package pl.lodz.sda;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

public class MySQLConnector {
    private static String USER;
    private static String PASSWORD;
    private static final String URL = "jdbc:mysql://localhost:3306/";
    private static String DB;

    public MySQLConnector(String fileName) {
        Properties properties = new Properties();
        try (InputStream inputStream = getClass().getClassLoader()
                .getResourceAsStream(fileName)) {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        USER = properties.getProperty("user");
        PASSWORD = properties.getProperty("password");
        DB = properties.getProperty("database");
    }

    public Connection connect() {
        try {
            return DriverManager.getConnection(URL + DB+"?useSSL=false&serverTimezone=UTC", USER, PASSWORD);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public static void main(String[] args) {
        try (Connection connection = new MySQLConnector("mysql.properties").connect();
                CSVPrinter printer = new CSVPrinter(new FileWriter("Filia.csv"), CSVFormat.DEFAULT))
        {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM Filia");
            printer.printRecords(resultSet);
            while (resultSet.next()) {
                System.out.println(resultSet.getString("Adres") + " "
                        + resultSet.getString("Nazwa"));
            }
            statement.close();
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
