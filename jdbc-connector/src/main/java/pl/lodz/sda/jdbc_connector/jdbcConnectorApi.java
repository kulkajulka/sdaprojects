package pl.lodz.sda.jdbc_connector;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface jdbcConnectorApi<E> {
    int[] batchInsert(List<E> list, Connection connection) throws SQLException;
    List<E> parseResponse(ResultSet resultSet) throws SQLException;
}
