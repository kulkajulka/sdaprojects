package pl.lodz.sda.dao;

import java.sql.Date;

public class Employee {
    private int id;
    private Date birth_date;
    private Date hire_date;
    private String first_name;
    private String last_name;

    public Employee(int id, Date birth_date,
                    Date hire_date, String first_name,
                    String last_name, char gender) {
        this.id = id;
        this.birth_date = birth_date;
        this.hire_date = hire_date;
        this.first_name = first_name;
        this.last_name = last_name;
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", birth_date=" + birth_date +
                ", hire_date=" + hire_date +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", gender=" + gender +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (id != employee.id) return false;
        if (gender != employee.gender) return false;
        if (!birth_date.equals(employee.birth_date)) return false;
        if (!hire_date.equals(employee.hire_date)) return false;
        if (!first_name.equals(employee.first_name)) return false;
        return last_name.equals(employee.last_name);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + birth_date.hashCode();
        result = 31 * result + hire_date.hashCode();
        result = 31 * result + first_name.hashCode();
        result = 31 * result + last_name.hashCode();
        result = 31 * result + (int) gender;
        return result;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(Date birth_date) {
        this.birth_date = birth_date;
    }

    public Date getHire_date() {
        return hire_date;
    }

    public void setHire_date(Date hire_date) {
        this.hire_date = hire_date;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    char gender;
}
