package pl.lodz.sda;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class CSVReader {

    private static String PATH = "/media/julia/hdd/SDAProjects/jdbc-connector/src/main/resources";
    public static List<CSVRecord> readCSV(String fileName, CSVFormat csvFormat){
        List<CSVRecord> list = new ArrayList<>();
        try{
            Reader reader = new FileReader(Paths.get(PATH,fileName).toFile());
            CSVParser parser = new CSVParser(reader,csvFormat);

            for (CSVRecord row : parser){
                list.add(row);
            }

            reader.close();
            parser.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

}