package pl.lodz.sda;

import org.apache.commons.csv.CSVFormat;
import org.h2.tools.Csv;
import pl.lodz.sda.dao.Employee;
import pl.lodz.sda.mapper.EmployeeMapper;

import java.sql.*;
import java.util.List;

public class H2MemoryDatabaseExample {

    private static final String DB_CONNECTION = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
    private static final String DB_USER = "sa";
    private static final String DB_PASSWORD = "";

    public static void main(String[] args) //throws Exception
    {
        //try {
            /*insertWithStatement();
            insertWithPreparedStatement();*/
            insertTestData();
        //} catch (SQLException e) {
        //    e.printStackTrace();
        //}
    }

    private static void insertTestData() {
        Connection connection = getDBConnection();
        if (connection == null){
            throw new RuntimeException();
        }
        String createQuery = "CREATE TABLE EMPLOYEE" +
                "(id int primary key, "+
                "birth_date DATE, "+
                "first_name varchar(50), "+
                "last_name varchar(50), "+
                "gender char(1), "+
                "hire_date DATE)";
        String selectQuery = "SELECT * FROM EMPLOYEE";
        Statement stmt;
        try {
            stmt = connection.createStatement();
            System.out.println("createQuery: "+stmt.execute(createQuery));

            List<Employee> list = EmployeeMapper.mapCSVRecordToEmployee(
                    CSVReader.readCSV("load_employees.csv",
                    CSVFormat.DEFAULT.withFirstRecordAsHeader()));

            int[] count = new jdbcConnectorApiEmployee()
                    .batchInsert(list,connection);

            System.out.println("Insert returned values: "+count);


            ResultSet resultSet = stmt.executeQuery(selectQuery);
            System.out.println(resultSet.toString());
            CSVWriter.writeCSV("employees_saved.csv",CSVFormat.DEFAULT
                            .withHeader("id","birth_date","first_name","last_name","gender","hire_date"),
                    EmployeeMapper.mapEmployeeToCSVRecord(new jdbcConnectorApiEmployee()
                    .parseResponse(resultSet)));
            while(resultSet.next()){
                System.out.print("id: "+ resultSet.getInt("id")+
                        ", birth_date: "+resultSet.getString("birth_date")+
                        ", first_name: "+resultSet.getString("first_name")+
                        ", last_name: "+resultSet.getString("last_name")+
                        ", gender: "+resultSet.getString("gender"));
                System.out.println();
            }
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try{
                connection.close();
            } catch (SQLException e){
                e.printStackTrace();
            }
        }
    }

    private static void insertWithPreparedStatement() throws SQLException {
        Connection connection = getDBConnection();

        String CreateQuery = "CREATE TABLE PERSON(id int primary key, name varchar(255))";
        String InsertQuery = "INSERT INTO PERSON" + "(id, name) values" + "(?,?)";
        String SelectQuery = "select * from PERSON";

        try {
            connection.setAutoCommit(false);
            PreparedStatement createPreparedStatement = connection.prepareStatement(CreateQuery);
            createPreparedStatement.executeUpdate();
            createPreparedStatement.close();

            PreparedStatement insertPreparedStatement = connection.prepareStatement(InsertQuery);
            insertPreparedStatement.setInt(1, 1);
            insertPreparedStatement.setString(2, "Jose");
            insertPreparedStatement.executeUpdate();
            insertPreparedStatement.close();

            PreparedStatement selectPreparedStatement = connection.prepareStatement(SelectQuery);
            ResultSet rs = selectPreparedStatement.executeQuery();
            System.out.println("H2 In-Memory Database inserted through PreparedStatement");
            while (rs.next()) {
                System.out.println("Id " + rs.getInt("id") + " Name " + rs.getString("name"));
            }
            selectPreparedStatement.close();
            connection.commit();

        } catch (SQLException e) {
            System.out.println("Exception Message " + e.getLocalizedMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.close();
        }
    }

    private static void insertWithStatement() throws SQLException {
        Connection connection = getDBConnection();
        try {
            connection.setAutoCommit(false);
            Statement stmt = connection.createStatement();
            stmt.execute("CREATE TABLE PERSON(id int primary key, name varchar(255))");
            stmt.execute("INSERT INTO PERSON(id, name) VALUES(1, 'Anju')");
            stmt.execute("INSERT INTO PERSON(id, name) VALUES(2, 'Sonia')");
            stmt.execute("INSERT INTO PERSON(id, name) VALUES(3, 'Asha')");

            ResultSet rs = stmt.executeQuery("select * from PERSON");
            System.out.println("H2 In-Memory Database inserted through Statement");
            while (rs.next()) {
                System.out.println("Id " + rs.getInt("id") + " Name " + rs.getString("name"));
            }

            stmt.execute("DROP TABLE PERSON");
            stmt.close();
            connection.commit();
        } catch (SQLException e) {
            System.out.println("Exception Message " + e.getLocalizedMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.close();
        }
    }

    private static Connection getDBConnection() {
        try {
            return DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
