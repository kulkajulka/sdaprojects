package pl.lodz.sda;

import pl.lodz.sda.dao.Employee;
import pl.lodz.sda.jdbc_connector.jdbcConnectorApi;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class jdbcConnectorApiEmployee implements jdbcConnectorApi<Employee> {
    @Override
    public int[] batchInsert(List<Employee> list, Connection connection) throws SQLException {
        PreparedStatement statement = connection
                .prepareStatement("INSERT INTO EMPLOYEE VALUES (?,?,?,?,?,?)");
        connection.setAutoCommit(false);
        list.forEach(e -> {
            try {
                statement.setInt(1, e.getId());
                statement.setDate(2, e.getBirth_date());
                statement.setString(3, e.getFirst_name());
                statement.setString(4, e.getLast_name());
                statement.setString(5, String.valueOf(e.getGender()));
                statement.setDate(6, e.getHire_date());
                statement.addBatch();
            } catch (SQLException ex) {
                System.err.println(ex.getSQLState());
            }
        });
        int[] count = statement.executeBatch();
        connection.commit();
        connection.setAutoCommit(true);
        statement.close();
        return count;
    }

    @Override
    public List<Employee> parseResponse(ResultSet resultSet) throws SQLException {
        List<Employee> newList = new ArrayList<>();
        while(resultSet.next()){
            newList.add(new Employee(resultSet.getInt("id"),
                    resultSet.getDate("birth_date"),
                    resultSet.getDate("hire_date"),
                    resultSet.getString("first_name"),
                    resultSet.getString("last_name"),
                    resultSet.getString("gender").charAt(0)));
        }
        return newList;
    }


}
