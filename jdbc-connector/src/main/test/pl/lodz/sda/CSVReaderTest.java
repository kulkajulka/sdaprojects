package pl.lodz.sda;

import org.apache.commons.csv.CSVFormat;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CSVReaderTest {

    @Test
    public void readerTest(){
        assertEquals(160,
                CSVReader.readCSV("load_employees.csv",
                        CSVFormat.DEFAULT.withFirstRecordAsHeader()).size());
    }
}