package com.julia;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sun.security.jgss.GSSCaller;

import java.io.*;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class GoldPricesApp {
    public final static String NBPAPI = "http://api.nbp.pl";

    public enum Rekomendacja{
        TRZYMAJ,SPRZEDAWAJ,KUPUJ
    }

    public static void main(String[] args) {

    File summary = Paths.get(System.getProperty("user.home"), "Pulpit", "Gold_Prices.csv").toFile();

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(NBPAPI)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    NBPService nbpService = retrofit.create(NBPService.class);

    try(Writer writer = new FileWriter(summary)) {
        Scanner scanner = new Scanner(System.in);
        print(execute(new QueryBuilder(ask(scanner), nbpService).build()),writer);
    } catch(IOException e){
            System.out.println("Nie udało się utworzyć pliku do zapisu danych!");
    }

}

    public static List<GoldPrice> execute(Call<List<GoldPrice>> call) throws IOException {
        Response<List<GoldPrice>> response = call.execute();
        if(response.isSuccessful()){
            return response.body();
        } else {
            System.err.println(response.errorBody());
            return Collections.emptyList();
        }
    }

    public static void print(List<GoldPrice> lista,Writer writer){
        if (lista.isEmpty()) {
            throw new IllegalArgumentException("Brak wyników zapytania!");
        }
        double średnia = lista.stream().map(a -> a.getCena())
                .reduce(0.0,(a,b) -> a+b)/lista.size();

        System.out.println("To nie jest porada inwestycyjna!");
        try (CSVPrinter printer = new CSVPrinter(writer, CSVFormat.RFC4180.withHeader("Data","Cena"))){
            lista.stream()
                    .forEach(g -> {
                        try {
                            System.out.println(g);
                            printer.printRecord(g.getData(), g.getCena());
                        } catch (IOException e) {

                        }
                    });
            printer.printRecord("średnia: ",średnia);
            if (lista.size()>3){
                double średnia3dni = lista.stream().skip(lista.size()-3)
                        .map(a -> a.getCena()).reduce(0.0,(a,b) -> a+b)/3;
                printer.printRecord("średnia3dni: ",średnia3dni);
                printer.printRecord("To nie jest porada inwestycyjna!");
                printer.printRecord(rekomendacja(średnia,średnia3dni));
            }


        }
        catch (IOException e){
            System.out.println("Nie udało się zapisać do pliku!");
        }
    }

    public static Rekomendacja rekomendacja(double srednia, double srednia3){
        if (srednia <= 0 || srednia3 <= 0) throw new IllegalArgumentException();
        if (Math.abs(1 -srednia/srednia3)*100 < 1) return Rekomendacja.TRZYMAJ;
        if (srednia3 > srednia) return Rekomendacja.SPRZEDAWAJ;
        else return Rekomendacja.KUPUJ;
    }

    public static int ask(Scanner scanner){
        System.out.print("To nie jest porada inwestycyjna!\nPodaj liczbę dni: ");
        String count = scanner.nextLine();
        try {
            return Integer.parseInt(count);
        } catch (NumberFormatException e){
            System.out.println("Podałeś nieprawidłowe dane!");
            throw new NumberFormatException("Nieprawidłowe dane!");
        }

    }
}
