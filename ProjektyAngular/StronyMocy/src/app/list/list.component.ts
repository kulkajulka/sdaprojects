import {Component, Input, OnInit, Output} from '@angular/core';
import {ItemComponent} from '../item/item.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  @Input() characters = Array<ItemComponent>();
  constructor() { }

  getCharacters(chosenTab) {
    if (chosenTab === 'all') {
      return this.characters.slice();
    }
    return this.characters.filter((char) => {
      return char.side === chosenTab;
    });
  }
  ngOnInit() {
  }

}
