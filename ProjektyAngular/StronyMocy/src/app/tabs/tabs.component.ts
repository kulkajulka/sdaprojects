import {Component, Input, OnInit} from '@angular/core';
import {ListComponent} from '../list/list.component';
import {ItemComponent} from '../item/item.component';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css']
})
export class TabsComponent implements OnInit {

  list = Array<ItemComponent>();
  chosenTab = 'all';

  constructor() { }

  ngOnInit() {
  }

  onChosenAll() {
    this.chosenTab = 'all';
  }
}
