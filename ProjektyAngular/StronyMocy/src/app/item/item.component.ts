import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  @Input() name = 'Julia';
  @Input() side = 'Light';
  @Output() setSide = new EventEmitter<string>();
  onDarkClick() {
    this.setSide.emit('Dark');
  }
  onLightClick() {
    this.setSide.emit('Light');
  }
  constructor() { }

  ngOnInit() {
  }

}
