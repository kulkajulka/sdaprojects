import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  tmp = '';
  @Input() items = ['Apples', 'Bananas', 'Cherries'];
  @Output() poKliknieciu = new EventEmitter<string>();
  @Output() poKliknieciuRemove = new EventEmitter();
  addItem() {
    this.poKliknieciu.emit(this.tmp);
  }
  removeItem() {
    this.poKliknieciuRemove.emit();
  }
  constructor() { }

  ngOnInit() {
  }
}
