import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  rootItems = ['Jeden', 'Dwa', 'Trzy'];
  poKliknieciuApp(newItem) {
    this.rootItems.push(newItem);
    console.log(this.rootItems);
  }
  poKliknieciuRemoveApp() {
    this.rootItems.pop();
    console.log(this.rootItems);
  }
}
