<%@ page import="julia.code.library.books.Book" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="julia.code.random.BookGenerator" %>
<%@ page import="julia.code.library.books.Type" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  Created by IntelliJ IDEA.
  User: julia
  Date: 02.10.17
  Time: 17:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Reserved Books</title>
</head>
<body>

<div style="border: dashed">
    <%@ include file="header.jsp"%>
</div>
<div style="float:left; width: 30%"><%@include file="menu.jsp"%></div><%--podaejmy ścieżkę wzgędem tego pliku--%>
<div style="float: right; width: 70%">
    <h3>Reserved Books</h3>
    <table style="text-align: center">
        <tr>
            <c:if test="${userLogin ne null}">
                <td>Imię</td>
                <td>Nazwisko</td>
                <td>Narodowść</td>
                <td>Id uzytkownika</td>
            </c:if>
            <td>Tytuł</td>
            <td>Author</td>
            <td>Id ksiązki</td>
        </tr>
        <c:forEach var="elem" items="${reservedBooks}" >
            <tr>
                <c:if test="${userLogin ne null}">
                    <td>${elem.getValue().getName()}</td>
                    <td>${elem.getValue().getSurname()}</td>
                    <td>${elem.getValue().getNatio()}</td>
                    <td>${elem.getValue().getId()}</td>
                </c:if>
                <td>${elem.getKey().getTitle()}</td>
                <td>${elem.getKey().getAuthor().toString()}</td>
                <td>${elem.getKey().getId()}</td>
            </tr>
        </c:forEach>
    </table>
</div>
<%-- komentarz --%>
<div style="clear: both"><p></p></div>

</body>
</html>
