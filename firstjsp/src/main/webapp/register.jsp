<%@ page import="julia.code.library.books.Book" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="julia.code.random.BookGenerator" %>
<%@ page import="julia.code.library.books.Type" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  Created by IntelliJ IDEA.
  User: julia
  Date: 02.10.17
  Time: 17:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Library</title>
</head>
<body>
<div style="border: dashed">
    <%@ include file="header.jsp"%>
</div>
<div style="float:left; width: 30%"><%@include file="menu.jsp"%></div><%--podaejmy ścieżkę wzgędem tego pliku--%>
<div style="float: right; width: 70%">
    <h3>Welcome to JSP Library</h3>
    <p>
        Nastapiło automatyczne wylogowanie!<br/>
        Podaj dane by zarejestrować nowego użytkownika.
    </p>
    <c:if test="${error ne null}">
        <p> Podano nieprawidłowe dane przy rejestracji!</p>
    </c:if>
    <form name="registerForm" action="/register"  method="post"> <%--/addBook będzie ściezka do metody w controllerze--%>
        <label>Imię użytkownika : </label><input type="text" name="name"><br/>
        <label>Nazwisko użytkownika : </label><input type="text" name="surname"><br/>
        <label>PESEL użytkownika : </label><input type="text" name="pesel"><br/>
        <label>Nationality:</label>
        <select name="natio">
            <c:forEach items="${nationalities}" var="elem">
                <option value="${elem}">${elem}</option>
            </c:forEach>
        </select><br/>
        <label>Passport: </label><input type="text" name="passport"><br/>
        <br/>
        <label>Login: </label><input type="text" name="login">
        <label>Password: </label><input type="text" name="password">
        <input type="submit" value="Zarejestruj">
    </form>

</div>
<%-- komentarz --%>
<div style="clear: both"><p></p></div>

</body>
</html>
