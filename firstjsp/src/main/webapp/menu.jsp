<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<ul>
    <c:forEach items="${menu}" var="elem">
        <a href="${elem.getMenuItemUri()}"><li>${elem.getMenuItemName()}</li></a>
    </c:forEach>
</ul>