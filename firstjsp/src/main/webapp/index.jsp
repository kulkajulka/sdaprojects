<%@ page import="julia.code.library.books.Book" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="julia.code.random.BookGenerator" %>
<%@ page import="julia.code.library.books.Type" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  Created by IntelliJ IDEA.
  User: julia
  Date: 02.10.17
  Time: 17:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Library</title>
</head>
<body>
<div style="border: dashed">
    <%@ include file="header.jsp"%>
</div>
<div style="float:left; width: 30%"><%@include file="menu.jsp"%></div><%--podaejmy ścieżkę wzgędem tego pliku--%>
<div style="float: right; width: 70%">
    <h3>Welcome to JSP Library</h3>
    <p>
        On left you can find menu.<br/>
        Use it to navigate on this JSP!
    </p>
<c:if test="${loggedUser ne null}">
    <form action="/logout" method="post">
        <input type="submit" value="Wyloguj">
    </form>
</c:if>
</div>
<%-- komentarz --%>
<div style="clear: both"><p></p></div>

</body>
</html>
