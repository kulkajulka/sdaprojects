<%@ page import="java.time.LocalDate" %>
<%@ page import="julia.code.random.RandomGenerator" %>
<%@ page import="java.util.Random" %>
<%@ page import="java.util.stream.Stream" %>
<%--
  Created by IntelliJ IDEA.
  User: julia
  Date: 29.09.17
  Time: 18:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Moje pierwsze JSP!</title>
</head>
<body>
<h1>HelloWorld <%= LocalDate.now()%>! </h1>
<a href="helloworld2.jsp">Chcesz wiedzieć która godzina?</a>
<p>Zawartość żądania: </p>
<p>
    Host: <%= request.getHeader("Host")%>
    <br/>
    Host zawołany małymi literami: <%= request.getHeader("host")%>
    <br/>
    Typ metody : <%=request.getMethod()%>
    <br/>
    Parametry użytkowanika: <%=request.getQueryString()%>
    <br/>
    Response : <%=request.getHeaderNames()%>
    <br/>
    Numery Lotto: <%= RandomGenerator.drawLottoNumbers()%>
    <br/>
    Dzisiejsze typy EuroJackPot:
    <%
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        for (int i = 0; i < 5; i++) {
            if (i != 0) {
                sb.append(", ");
                sb2.append(", ");
            }
            if (i < 2) {
                sb2.append(random.nextInt(10) + 1);
            }
            sb.append(random.nextInt(50) + 1);
        }
        out.println(sb.toString());
        out.println("<br/> Oraz dwie z 10: ");
        out.println(sb2.toString());

        String userParams = request.getQueryString();
        if (userParams == null)
        {
            response.sendRedirect("http://google.com");
        }
        else
        {
            String[] params = userParams.split("&");
            String idParam = null;
            for (String param: params) {
                String[] keyValuePair = param.split("=");
                if (keyValuePair.length == 2 && keyValuePair[0].contains("id"))
                {
                    idParam = keyValuePair[1];
                    break;
                }
            }
            if (idParam == null)
            {
                response.sendError(404);
            }
            else
            {
                out.println("Wartość parametru id: "+idParam);
            }
        }
    %>
</p>
</body>
</html>
