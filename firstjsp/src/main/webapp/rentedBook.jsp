<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: julia
  Date: 04.10.17
  Time: 18:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Reservation Status</title>
</head>
<body>

<div style="border: dashed">
    <%@ include file="header.jsp"%>
</div>
<div style="float:left; width: 30%"><%@include file="menu.jsp"%></div><%--podajemy ścieżkę wzgędem tego pliku--%>
<div style="float: right; width: 70%">
    <h4>Wypożyczanie książki</h4>
    <c:choose>
    <c:when test="${status eq 200}">
        <p style="font-size: medium; border: cyan dotted">Książka została wypożyczona!</p>
    </c:when>
    <c:otherwise>
        <p style="font-size: medium; border: red dotted">Nie udało się wypożyczyć książki!</p>
    </c:otherwise>
    </c:choose>
</div>
<%-- komentarz --%>
<div style="clear: both"><p></p></div>

</body>
</html>

