<%@ page import="julia.code.library.books.Book" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="julia.code.random.BookGenerator" %>
<%@ page import="julia.code.library.books.Type" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  Created by IntelliJ IDEA.
  User: julia
  Date: 02.10.17
  Time: 17:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Book donating</title>
</head>
<body>

<div style="border: dashed">
    <%@ include file="header.jsp"%>
</div>


<div style="float:left; width: 30%"><%@include file="menu.jsp"%></div><%--podajemy ścieżkę wzgędem tego pliku--%>
<div style="float: right; width: 70%">
    <%
        Cookie[] cookies = request.getCookies();

        if (cookies.length ==0) {
            out.println("Brak ostatnio dodanych książek!");
        }
        else {
            out.println("Książki ostatnio dodane: ");
            for (Cookie cookie : cookies) {
                if (cookie.getName().equalsIgnoreCase("JSESSIONID")) {
                    continue;
                }
                out.println(cookie.getName()+" - "+cookie.getValue());
            }
        }
    %>
    <h4>Dodawanie nowej pozycji w bibliotece</h4>
    <form name="bookForm" action="/addBook" method="post"> <%--/addBook będzie ściezka do metody w controllerze--%>
        <label>Nazwa książki: </label><input type="text" name="title"><br/>
        <label>Imię autora książki: </label><input type="text" name="Author.name"><br/>
        <label>Nazwisko autora książki: </label><input type="text" name="Author.surname"><br/>
        <label>Liczba stron: </label><input type="text" name="pageCount"><br/>
        <label>ISBN: </label><input type="text" name="isbn"><br/>
        <label>Type: </label>
        <select name="type">
            <c:forEach items="${booksTypes}" var="elem">
                <option value="${elem}">${elem.getFriendlyName()}</option>
            </c:forEach>
        </select>
        <br/>
        <input type="submit" value="Zapisz">
    </form>



</div>
<%-- komentarz --%>
<div style="clear: both"><p></p></div>

</body>
</html>
