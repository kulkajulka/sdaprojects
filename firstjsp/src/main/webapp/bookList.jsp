<%@ page import="julia.code.library.books.Book" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="julia.code.random.BookGenerator" %>
<%@ page import="julia.code.library.books.Type" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  Created by IntelliJ IDEA.
  User: julia
  Date: 02.10.17
  Time: 17:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Booklist</title>
</head>
<body>

<div style="border: dashed">
    <%@ include file="header.jsp"%>
</div>
<div style="float:left; width: 30%"><%@include file="menu.jsp"%></div><%--podaejmy ścieżkę wzgędem tego pliku--%>
<div style="float: right; width: 70%">
    <h3>Booklist</h3>
    <table style="text-align: center">
        <tr>
            <td>ID</td>
            <td>Author</td>
            <td>Title</td>
            <td>ISBN</td>
            <td>Type</td>
            <td>Page Count</td>
            <td>Options</td>
        </tr>
        <c:forEach var="book" items="${list}" >
            <tr>
                <td>${book.getId()}</td>
                <td>${book.getAuthor().toString()}</td>
                <td>${book.getTitle()}</td>
                <td>${book.getIsbn()}</td>
                <td>${book.getType().getFriendlyName()}</td>
                <td>${book.getPageCount()}</td>
                <td><a href="/books/rent?id=${book.getId()}">Rserve book</a> </td>
            </tr>
        </c:forEach>
    </table>
</div>
<%-- komentarz --%>
<div style="clear: both"><p></p></div>

</body>
</html>
