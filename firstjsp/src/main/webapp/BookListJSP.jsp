<%@ page import="julia.code.library.books.Book" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="julia.code.random.BookGenerator" %>
<%@ page import="julia.code.library.books.Type" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  Created by IntelliJ IDEA.
  User: julia
  Date: 02.10.17
  Time: 17:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Book List</title>
</head>
<body>
<%! int requestCount = 0; %>

<div style="border: dashed">
    <p>
        Tytuł strony
    </p>
</div>
<div style="float:left; width: 30%"><%@include file="menu.jsp"%></div><%--podaejmy ścieżkę wzgędem tego pliku--%>
<div style="float: right; width: 70%">
<p>Content strony</p>
   <%-- <p>

        <%

        out.println("<table>");
        out.println("<tr style=\"text-align: center\"><td>Title</td><td>Author</td><td>ISBN</td></tr>");
            for (Book book: bookList) {
                out.println("<tr>");
                    out.println(String.format("<td>%s</td><td>%s</td><td>%s</td>",book.getTitle(),
                            book.getAuthor().toString(),book.getIsbn()));
                out.println("</tr");
            }


        out.println("</table");
        %>
    </p>
--%>
    <table>
        <tr>
            <td>Author</td>
            <td>Title</td>
            <td>ISBN</td>
        </tr>
    <c:forEach var="book" items="${list}" >
        <tr>
            <td>${book.getAuthor().toString()}</td>
            <td>${book.getTitle()}</td>
            <td>${book.getIsbn()}</td>
        </tr>
        </c:forEach>
    </table>

    <p>Liczba wywołań strony: <%= ++requestCount%></p>
    <p>Liczba wyświetleń strony: ${5+4}</p>

</div>
<%-- komentarz --%>
<div style="clear: both"><p></p></div>

</body>
</html>
