<%@ page import="julia.code.library.books.Book" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="julia.code.random.BookGenerator" %>
<%@ page import="julia.code.library.books.Type" %>
<%@ page import="julia.code.library.members.Members" %>
<%@ page import="julia.code.library.members.LibraryMember" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  Created by IntelliJ IDEA.
  User: julia
  Date: 02.10.17
  Time: 17:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Book renting</title>
</head>
<body>

<div style="border: dashed">
    <%@ include file="header.jsp"%>
</div>
<div style="float:left; width: 30%"><%@include file="menu.jsp"%></div><%--podajemy ścieżkę wzgędem tego pliku--%>
<div style="float: right; width: 70%">
    <c:if test="${validationError eq true}">
        <h4 style="color: crimson">Podano nieprawidłowe dane!</h4>
    </c:if>

<%--    <form name="bookRentAs" action="/rentBookAs" method="post">
    <%! LibraryMember member; %>
    <%
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("memberId")) {
                Integer previousUserId = Integer.parseInt(cookie.getValue());
                if (previousUserId != null)
                    member = Members.getMember(previousUserId);

            }
        }
        if (member != null){
            out.println("<p>Czy chcesz wypożyczyć tę książkę jako użytkownik: "+member.toString());
            out.println("<input type=\"hidden\" name=\"memberId\" value=\""+member.getId()+"\">");
            out.println("<input type=\"submit\" value=\"Rezerwuj\">");
        }
    %>
        <input type="hidden" name="bookId" value="${book.getId()}">


        </form>--%>

    <h4>The book you want to rent: </h4>
    <table style="text-align: center">
        <tr>
            <td>ID</td>
            <td>Author</td>
            <td>Title</td>
            <td>ISBN</td>
            <td>Type</td>
            <td>Page Count</td>
            <td>Options</td>
        </tr>
            <tr>
                <td>${book.getId()}</td>
                <td>${book.getAuthor().toString()}</td>
                <td>${book.getTitle()}</td>
                <td>${book.getIsbn()}</td>
                <td>${book.getType().getFriendlyName()}</td>
                <td>${book.getPageCount()}</td>
            </tr>

    </table>
        <form name="bookRentForm" action="/rentBook"  method="post"> <%--/addBook będzie ściezka do metody w controllerze--%>
            <input type="hidden" name="bookId" value="${book.getId()}">
           <%--%> <label>Imię użytkownika : </label><input type="text" name="name"><br/>
            <label>Nazwisko użytkownika : </label><input type="text" name="surname"><br/>
            <label>PESEL użytkownika : </label><input type="text" name="pesel"><br/>
            <label>Nationality:</label>
            <select name="natio">
                <c:forEach items="${nationalities}" var="elem">
                    <option value="${elem}">${elem}</option>
                </c:forEach>
            </select><br/>
            <label>Passport: </label><input type="text" name="passport"><br/>
            <br/>--%>
            <input type="submit" value="Rezerwuj">
        </form>
</div>
<%-- komentarz --%>
<div style="clear: both"><p></p></div>

</body>
</html>
