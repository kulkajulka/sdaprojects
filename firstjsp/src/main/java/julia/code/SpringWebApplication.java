package julia.code;

import julia.code.database.H2Connector;
import julia.code.filters.ActiveSessionFilter;
import julia.code.filters.PresentDataFilter;
import julia.code.library.members.LibraryMember;
import julia.code.library.members.Members;
import julia.code.library.members.Nationalities;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import javax.swing.*;

@SpringBootApplication
@ComponentScan(basePackages = {"julia.code"})
public class SpringWebApplication extends SpringBootServletInitializer{

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SpringWebApplication.class);
    }

    public static void main(String[] args) throws Exception {
        Members.addMember(new LibraryMember("Julia","Kowalska","98746156", Nationalities.PL,
                "admin","admin"));
        H2Connector.loadDB();
        SpringApplication.run(SpringWebApplication.class, args);

    }

    @Bean
    public FilterRegistrationBean loginFilterRegistration() {

        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new PresentDataFilter());
        registration.addUrlPatterns("/login");
        registration.setName("loginFilter");
        registration.setOrder(2);

        return registration;
    }
    @Bean
    public FilterRegistrationBean activeSessionFilterRegistration() {

        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new ActiveSessionFilter());
        registration.addUrlPatterns("/login");
        registration.setName("activeSessionFilter");
        registration.setOrder(1);

        return registration;
    }
}
