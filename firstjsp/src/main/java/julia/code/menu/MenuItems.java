package julia.code.menu;

public enum MenuItems {
    INDEX("Strona główna","/"),BOOK_LIST("Lista książek","/books"),
    BOOK_DONATION("Dodawanie książek","/books/add"),MEMBERS("Członkowie biblioteki", "/members.csv"),
    BOOK_RESERVATIONS("Rezerwacje książek","/reserved"),LOGIN("Zaloguj","/login"),REGISTER("Zarejestruj","/register");

    private String menuItemName;
    private String menuItemUri;

    MenuItems(String menuItem,String uri) {
        this.menuItemName = menuItem;
        this.menuItemUri = uri;
    }

    public String getMenuItemName() {
        return menuItemName;
    }

    public String getMenuItemUri() {
        return menuItemUri;
    }
}
