package julia.code.model;

import julia.code.menu.MenuItems;
import org.springframework.web.servlet.ModelAndView;

public class ModelAndViewWithMenu extends ModelAndView{
    public ModelAndViewWithMenu() {
        super();
        this.addObject("menu", MenuItems.values());
    }
}
