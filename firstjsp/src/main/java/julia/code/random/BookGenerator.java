package julia.code.random;

import julia.code.library.books.Author;
import julia.code.library.books.Book;
import julia.code.library.books.Type;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BookGenerator {
    private final static String[] names = new String[]
            {"Julia","Piotr","Adam","Daniel","Marek","Katarzyna"};
    private final static String[] surnames = new String[]
            {"Johnson","Nowak","Mytych","Kowalski","Szot","Kernel"};
    private final static String[] titles = new String[] {
            "Pan Tadeusz po polsku","Java Podstawy","Czysty kod","Kuchnia polska","Malowany welon"};
    private final static String[] NATIONALIETIES = new String[]
            {"Polish","German","British","Swedish","Italian"};
    public static Book createBook(String title,String authorName,String authorSurname){
        Random random = new Random();
        Author author = new Author(authorName,authorSurname,
                NATIONALIETIES[random.nextInt(NATIONALIETIES.length)]);
        Type[] booktypes = Type.values();
        Book book = new Book(title,generateISBN(random), random.nextInt(1500),
                author, booktypes[random.nextInt(booktypes.length)]);
        return book;
    }

    private static String generateISBN(Random random){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            sb.append(random.nextInt(10));
        }
        return sb.toString();
    }

    public static List<Book> generateBookList(int size){
        List<Book> bookList = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            bookList.add(createBook(titles[random.nextInt(titles.length)],
                    names[random.nextInt(names.length)],
                    surnames[random.nextInt(surnames.length)]));
        }
        return bookList;
    }
}
