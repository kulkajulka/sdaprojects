package julia.code.random;

import java.util.Random;

public class RandomGenerator {
    public static String drawLottoNumbers(){
        Random random = new Random();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 1; i <= 6; i++)
        {
            if (i != 1)
            {
                stringBuilder.append(", ");
            }
            stringBuilder.append(random.nextInt(49)+1);
        }
        return stringBuilder.toString();
    }
}
