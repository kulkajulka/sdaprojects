package julia.code.controller;

import julia.code.library.books.Repository;
import julia.code.library.members.LibraryMember;
import julia.code.library.members.Members;
import julia.code.model.ModelAndViewWithMenu;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class BookRentController {

    @RequestMapping(value = "/books/rent")
    public ModelAndView rentBook(@RequestParam("id") String id,
                                 @RequestParam(name ="error",required = false) Boolean error,
                                 HttpServletRequest request){
        HttpSession session = request.getSession();
        ModelAndView model = new ModelAndViewWithMenu();
        String userLogin = (String) session.getAttribute("UserLogin");
        if (userLogin != null) {
            model.addObject("loggedUser",userLogin);
            model.addObject("book", Repository.getBook(Integer.parseInt(id)));
            model.setViewName("../rentBook.jsp");
            if (error != null && error) {
                model.addObject("validationError",true);
            }
            return model;
        }
        model.setViewName("../login.jsp");
        StringBuilder backUrl = new StringBuilder();
        backUrl.append(request.getRequestURI());
        backUrl.append("?");
        backUrl.append(request.getQueryString());
        session.setAttribute("backUrl",backUrl.toString());
        return model;
    }

    @RequestMapping(value = "/rentBook", method = RequestMethod.POST)
    public String rentBook(HttpServletRequest request,
                           @ModelAttribute("bookId") String bookId,
                           HttpServletResponse response){
        HttpSession session = request.getSession();
        String userLogin = (String) session.getAttribute("UserLogin");
        LibraryMember libraryMember = Members.getMember(userLogin);
        /*if (libraryMember.getNatio() == Nationalities.PL) {
            if (libraryMember.getPesel().equals("")) return "redirect:/books/rent?id="+bookId+"&error=true";
        }
        else if (libraryMember.getPassport().equals("")) return "redirect:/books/rent?id="+bookId+"&error=true";*/

        int status = Repository.reserveBook(Repository.getBook(Integer.parseInt(bookId)),
            libraryMember);
        /*if (status == 200) {
            Members.addMember(libraryMember);
        }*/

        Cookie memberIdCookie = new Cookie("memberId",libraryMember.getId().toString());
        /*memberIdCookie.setMaxAge(60);*/
        response.addCookie(memberIdCookie);
        return "redirect:/rentresult?status="+status;

    }

    @RequestMapping(value = "/rentresult")
    public ModelAndView getRentResult(@RequestParam(name ="status") Integer status){
        ModelAndView model = new ModelAndViewWithMenu();
        model.setViewName("rentedBook.jsp");
        model.addObject("status",status);
        return model;
    }

    @RequestMapping(value = "/rentBookAs", method = RequestMethod.POST)
    public String rentBookAs(@ModelAttribute("memberId") String memberId,
                             @ModelAttribute("bookId") String bookId,
                             HttpServletResponse response) {

        LibraryMember member = Members.getMember(Integer.parseInt(memberId));
        int status = Repository.reserveBook(Repository.getBook(Integer.parseInt(bookId)),
                member);

        Cookie memberIdCookie = new Cookie("memberId",memberId);
        memberIdCookie.setMaxAge(60);
        response.addCookie(memberIdCookie);
        return "redirect:/rentresult?status="+status;
    }

    @RequestMapping(value = "/reserved")
    public ModelAndView getMembers(HttpServletRequest request){
        ModelAndView model = new ModelAndViewWithMenu();
        model.addObject("userLogin",
                (String) request.getSession().getAttribute("UserLogin"));
        model.addObject("reservedBooks",Repository.getReservedBooks());
        model.setViewName("reserved.jsp");
        return model;
    }
}
