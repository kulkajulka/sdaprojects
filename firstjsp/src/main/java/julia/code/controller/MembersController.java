package julia.code.controller;

import julia.code.database.H2Connector;
import julia.code.library.members.LibraryMember;
import julia.code.library.members.Members;
import julia.code.library.members.Nationalities;
import julia.code.model.ModelAndViewWithMenu;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class MembersController {
    @RequestMapping(value = "/members")
    public ModelAndView getMembers(HttpServletRequest request){
        HttpSession session = request.getSession();
        String userLogin = (String) session.getAttribute("UserLogin");
        ModelAndView model = new ModelAndViewWithMenu();
        if (userLogin != null){
            model.addObject("members", Members.getMembers()/*H2Connector.getMembers()*/);
            model.setViewName("members.jsp");
            return model;
        }
        model.setViewName("login.jsp");
        StringBuilder backUrl = new StringBuilder();
        backUrl.append(request.getRequestURI());
        backUrl.append("?");
        backUrl.append(request.getQueryString());
        session.setAttribute("backUrl",backUrl.toString());
        return model;
    }
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(@ModelAttribute("registerForm") LibraryMember newMember,
                           HttpServletRequest request) {
        if (newMember.getNatio() == Nationalities.PL) {
            if (newMember.getPesel().equals("")) return "redirect:/register?error=true";
        }
        else if (newMember.getPassport().equals("")) return "redirect:/register?error=true";

        Members.addMember(newMember);
        return "redirect:/login";
    }
}
