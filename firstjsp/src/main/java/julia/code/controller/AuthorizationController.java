package julia.code.controller;

import julia.code.library.members.LibraryMember;
import julia.code.library.members.Members;
import julia.code.library.members.Nationalities;
import julia.code.model.ModelAndViewWithMenu;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class AuthorizationController {

    @RequestMapping(value = "/anonymous")
    public ModelAndView anonymousPage() {
        ModelAndView model = new ModelAndViewWithMenu();
        model.addObject("loggedUser", "anonymous");
        model.setViewName("anonymous.jsp");
        return model;
    }

    @RequestMapping("/authenticated")
    public ModelAndView authorizedPage(HttpServletRequest request) {
        /*obiekt request posiada dane z przeglądarki np ciasteczka,
        parametry zapytania, to co wprowadzi użytkownik, np w formularzu*/
        ModelAndView model = new ModelAndViewWithMenu();
        HttpSession session = request.getSession(false);
        String userLogin = (String) session.getAttribute("UserLogin");
        model.addObject("loggedUser", userLogin);
        model.setViewName("anonymous.jsp");
        return model;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@ModelAttribute("login") String login,
                        @ModelAttribute("password") String password,
                        HttpServletRequest request) {
        LibraryMember member = Members.getMember(login);
        if (member != null) {
            if (member.getPassword().equals(password)) {
                HttpSession session = request.getSession();
                session.setMaxInactiveInterval(15);
                session.setAttribute("UserLogin", login);
                String backurl = (String) session.getAttribute("backUrl");
                if (backurl != null) {
                    return "redirect:" + backurl;
                } else return "redirect:/";
            }
        }
        return "redirect:/login?error=nomember";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public String logout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.invalidate();
        return "redirect:/";
    }

    @RequestMapping("/login")
    public ModelAndView logIn(HttpServletRequest request,
                              @RequestParam(value = "error", required = false) String error) {
        ModelAndView model = new ModelAndViewWithMenu();
        model.addObject("error", error);
        model.setViewName("login.jsp");
        return model;
    }

    @RequestMapping("/register")
    public ModelAndView registerForm(HttpServletRequest request,
                                     @RequestParam(value = "error", required = false) String error) {
        ModelAndView model = new ModelAndViewWithMenu();
        model.setViewName("register.jsp");
        request.getSession().invalidate();
        model.addObject("error", error);
        model.addObject("nationalities", Nationalities.values());
        return model;

    }
}
