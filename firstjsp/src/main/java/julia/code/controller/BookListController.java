package julia.code.controller;

import julia.code.library.books.Author;
import julia.code.library.books.Book;
import julia.code.library.books.Repository;
import julia.code.library.books.Type;
import julia.code.menu.MenuItems;
import julia.code.model.ModelAndViewWithMenu;
import julia.code.random.BookGenerator;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class BookListController {

    @RequestMapping(value = "")
    public ModelAndView mainPage(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        ModelAndView model = new ModelAndViewWithMenu();
        if (session != null) {
            String userLogin = (String) session.getAttribute("UserLogin");
            if (userLogin != null) {
                model.addObject("loggedUser",userLogin);
            }
        }

        model.setViewName("index.jsp");
        return model;
    }

    @RequestMapping(value = "/books")
    public ModelAndView getBooks(){
        ModelAndView model = new ModelAndViewWithMenu();
        model.addObject("list", Repository.getBooks());
        model.setViewName("bookList.jsp");
        return model;
    }

    @RequestMapping(value = "/books/add")
    public ModelAndView addBook(HttpServletRequest request){
        HttpSession session = request.getSession();
        ModelAndView model = new ModelAndViewWithMenu();

        String userLogin = (String) session.getAttribute("UserLogin");
        if (userLogin != null) {
            model.addObject("loggedUser",userLogin);
            model.setViewName("../addBook.jsp");
            model.addObject("booksTypes", Type.values());
            return model;
        }
        StringBuilder backUrl = new StringBuilder();
        backUrl.append(request.getRequestURI());
        backUrl.append("?");
        backUrl.append(request.getQueryString());
        session.setAttribute("backUrl",backUrl.toString());
        model.setViewName("../login.jsp");
        return model;
    }


}
