package julia.code.controller;

import julia.code.library.books.Book;
import julia.code.library.books.Repository;
import julia.code.model.ModelAndViewWithMenu;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class BookSaveController {

    @RequestMapping(value = "/addBook", method = RequestMethod.POST)
    public String addBook(@ModelAttribute("bookForm") Book book,
                          HttpServletRequest request,
                          HttpServletResponse response){
        Cookie cookie1 = new Cookie("BookTitle",book.getTitle());
        cookie1.setMaxAge(30);
        Cookie cookie2 = new Cookie("BookAuthor",book.getAuthor().getSurname());
        Cookie cookie3 = new Cookie("BookType",book.getIsbn());
        response.addCookie(cookie1);
        response.addCookie(cookie2);
        response.addCookie(cookie3);
        return "redirect:/saveresult?status="+Repository.addBook(book);
    }

    @RequestMapping(value = "/saveresult")
    public ModelAndView getSaveResult(@RequestParam("status") Integer status){
        ModelAndView model = new ModelAndViewWithMenu();
        model.setViewName("addedBook.jsp");
        model.addObject("status",status);
        return model;
    }
}
