package julia.code.library.books;

public enum Type {
    ROMANCE("Romans"),DOCUMENT("Dokument"),THRILLER("Thriller"),COMEDY("Komedia"),DRAMA("Dramat");

    private String friendlyName;

    Type(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }
}
