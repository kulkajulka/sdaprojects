package julia.code.library.books;

import julia.code.library.members.LibraryMember;

import java.util.*;

public class Repository {
    private static List<Book> books = new ArrayList<>();
    private static Map<Book,LibraryMember> reservedBooks = new HashMap<>();

    public static Integer reserveBook(Book book,LibraryMember libraryMember){
        if (reservedBooks.containsKey(book)) {
            return 404;
        } else {
            reservedBooks.put(book,libraryMember);
            return 200;
        }
    }

    public static Map<Book,LibraryMember> getReservedBooks(){
        return reservedBooks;
    }
    public static int addBook(Book book){
        if (!books.contains(book)) {
            book.setId(books.size());
            books.add(book);
            return 200;
        }
        return 400;
    }

    public static List<Book> getBooks() {
        return books;
    }

    public static Book getBook(Integer id){
        return books.stream().filter(el -> el.getId() == id).findFirst().get();
    }
}
