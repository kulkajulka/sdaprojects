package julia.code.library.members;

import java.util.ArrayList;
import java.util.List;

public class Members {
    private static List<LibraryMember> members = new ArrayList<>();

    public static void addMember(LibraryMember libraryMember) {
        if (!members.contains(libraryMember))
        {
            libraryMember.setId(members.size());
            members.add(libraryMember);
        }
    }

    public static List<LibraryMember> getMembers() {
        return members;
    }

    public static LibraryMember getMember(Integer id) {
        return members.stream().filter(mem -> mem.getId() == id).findFirst().orElse(null);
    }

    public static LibraryMember getMember(String login) {
        return members.stream().filter(mem -> mem.getLogin().equals(login)).findFirst().orElse(null);
    }
}
