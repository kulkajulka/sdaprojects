package julia.code.library.members;

public enum Nationalities {
    PL("PL"),GER("GER"),GB("GB"),IT("IT"),OTHER("OTHER");
    private String natio;
    Nationalities(String natio) {
        this.natio = natio;
    }
}
