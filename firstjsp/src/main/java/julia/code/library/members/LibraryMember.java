package julia.code.library.members;

import javax.swing.*;

public class LibraryMember {
    private String name;
    private String surname;
    private String pesel;
    private Nationalities natio;
    private String passport;
    private Integer id;
    private String login;
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public Nationalities getNatio() {
        return natio;
    }

    public void setNatio(Nationalities natio) {
        this.natio = natio;
    }

    public LibraryMember(String name, String surname, String pesel,
                         Nationalities natio,String login,String password) {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
        this.natio = natio;
        this.login = login;
        this.password =password;
    }

    public LibraryMember() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LibraryMember that = (LibraryMember) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (surname != null ? !surname.equals(that.surname) : that.surname != null) return false;
        if (pesel != null ? !pesel.equals(that.pesel) : that.pesel != null) return false;
        if (natio != that.natio) return false;
        return passport != null ? passport.equals(that.passport) : that.passport == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (pesel != null ? pesel.hashCode() : 0);
        result = 31 * result + (natio != null ? natio.hashCode() : 0);
        result = 31 * result + (passport != null ? passport.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return name+" "+surname+" "+(pesel.equals(null) ? passport : pesel);
    }
}
