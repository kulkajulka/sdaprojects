package julia.code.database;

import jdk.internal.dynalink.linker.LinkerServices;
import julia.code.library.members.LibraryMember;
import julia.code.library.members.Nationalities;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class H2Connector {

    private static final String DB_CONNECTION = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
    private static final String DB_USER = "library";
    private static final String DB_PASSWORD = "";
    private static final String createMembersTable = "CREATE TABLE MEMBERS "+
            "(id int primary key, "+
            "name varchar(50), "+
            "surname varchar(50), "+
            "login varchar(50), "+
            "password varchar(50), "+
            "pesel varchar(50), "+
            "passport varchar(50), "+
            "nationality varchar(50))";
    private static File csvMembers = Paths
            .get("/media/julia/hdd/SDAProjects/firstjsp/src/main/resources/members.csv")
            .toFile();
    private static String GET_MEMBERS = "SELECT * FROM MEMBERS";

    private static Connection getDBConnection() {
        try {
            return DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
    public static void loadDB(){
        Connection con = getDBConnection();
        Statement statement;
        try
        {
            statement = con.createStatement();
            statement.execute(createMembersTable);
            statement.close();
            insertMembers(con);
            System.out.println("DB loaded");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static int[] insertMembers(Connection connection){
        int[] inserted = null;
        try (Reader reader = new FileReader(csvMembers)) {
            try (CSVParser csvParser = new CSVParser(reader,CSVFormat.DEFAULT
                    .withFirstRecordAsHeader())) {
                connection.setAutoCommit(false);
                PreparedStatement stmt = connection
                        .prepareStatement("INSERT INTO MEMBERS VALUES (?,?,?,?,?,?,?,?)");
                for (CSVRecord row : csvParser) {
                    stmt.setInt(1,Integer.parseInt(row.get("id")));
                    stmt.setString(2,row.get("name"));
                    stmt.setString(3,row.get("surname"));
                    stmt.setString(4,row.get("login"));
                    stmt.setString(5,row.get("password"));
                    stmt.setString(6,row.get("pesel"));
                    stmt.setString(7,row.get("passport"));
                    stmt.setString(8,row.get("nationality"));
                    stmt.addBatch();
                }
                inserted = stmt.executeBatch();
                connection.commit();
                connection.setAutoCommit(true);
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Inserted: "+inserted);
        return inserted;
    }

    public static List<LibraryMember> getMembers() {
        List<LibraryMember> members = new ArrayList<>();
        Connection connection = getDBConnection();
        try {
            Statement stmt = connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(GET_MEMBERS);
            while (resultSet.next()) {
                LibraryMember member = new LibraryMember(resultSet.getString("name"),
                        resultSet.getString("surname"),
                        resultSet.getString("pesel"),
                        Nationalities.valueOf(resultSet.getString("nationality")),
                        resultSet.getString("login"),
                        resultSet.getString("password"));
                member.setId(resultSet.getInt("id"));
                members.add(member);
            }
            resultSet.close();
            stmt.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return members;
    }
}

