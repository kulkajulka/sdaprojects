package hello;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {
    
    @RequestMapping("/julia")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @RequestMapping("/dupa")
    public int mojaMetoda(){
        return 404*2;
    }
}
