package julia.code.weatherApp;

import org.apache.http.client.utils.URIBuilder;

public interface Query {
    void applyToURIBuilder(URIBuilder uri);
}
