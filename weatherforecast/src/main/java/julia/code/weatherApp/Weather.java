package julia.code.weatherApp;

import com.google.gson.Gson;
import julia.code.model.WeatherModel;
import org.apache.http.client.utils.URIBuilder;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;
import java.util.Scanner;


public class Weather {
    final static String apiKey = "f54825f641048fa5174b5d25cc7d514d";
    final static String host = "api.openweathermap.org";
    final static String path = "/data/2.5/weather";
    final static Scanner in = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Pogoda");
        while(true) {
            buildUri(host, path, apiKey, requestQueryType())
                    .flatMap(Weather::sendReceive)
                    .flatMap(Weather::parseWeather)
                    .ifPresent(Weather::printWeather);
        }
        // flatMap stosujemy do łączenia funkcji o różnych wejściach i wyjściach, wszystkie w łańcuszku muszą
        // mieć ten sam rodzaj niejasności (Optional)
    }

    private static Query requestQueryType(){
        System.out.println("Typ zapytania: \n1. Miasto.\n2. Współrzędne.\n3. EXIT");
        int choice = in.nextInt();
        in.nextLine();
        switch (choice){
            case 1:
                System.out.print("Podaj miasto lub miasto i kod w formacie miasto,kod:");
                String cityCode = in.nextLine();
                return new CityQuery(cityCode);
            case 2:
                System.out.print("Podaj koordynaty:");
                return new GeoQuery(in.nextDouble(),in.nextDouble());
            case 3:
                System.exit(0);
            default: return new CityQuery("Lodz");
        }
    }

    private static void printWeather(WeatherModel weather) {
        System.out.println(weather);                       // do zmodyfikowania by wyświetlało dokładniejszą pogodę
    }
    private static Optional<WeatherModel> parseWeather(String gson){
        return Optional.of(new Gson().fromJson(gson,WeatherModel.class));
    }
    private static Optional<String> sendReceive(URI uri){
        return new HttpGetRequest(uri).go();
    }

    private static Optional<URI> buildUri(String host,String path,String apiKey,Query cQuery) {
        URIBuilder uri;    // można też zamienic na Optional<URI>
        try {
             uri = new URIBuilder().setScheme("http").setHost(host).setPath(path)
                    .setParameter("lang", "pl")
                    .setParameter("units", "metric").setParameter("appid", apiKey);
            cQuery.applyToURIBuilder(uri);
            return Optional.of(uri.build());
        } catch (URISyntaxException e) {
            System.err.println(e.getMessage());
            return Optional.empty();
        }
    }
    /*private static String readURL(URI uri) throws IOException {
        String myGson;
        try(CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpGet httpget = new HttpGet(uri);
            //System.out.println(httpget.getURI());
            //System.out.println(httpget.getRequestLine());
            myGson = getJsonString(httpclient, httpget);
        }
        return myGson;
    }

    private static String getJsonString(CloseableHttpClient httpclient, HttpGet httpget) throws IOException {
        String myGson;
        try(CloseableHttpResponse response1 = httpclient.execute(httpget)){
            System.out.println(response1.getStatusLine());
            HttpEntity entity1 = response1.getEntity();
            // do something useful with the response body
            // and ensure it is fully consumed
            myGson = EntityUtils.toString(entity1);
        }
        return myGson;
    }*/
}
