package julia.code.weatherApp;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.util.Optional;

public class HttpGetRequest {
    private final HttpGet httpGet;

    public HttpGetRequest(URI uri){
        httpGet = new HttpGet(uri);
    }

    public Optional<String> go(){
        try(CloseableHttpClient httpClient = HttpClients.createDefault()){
            return response(httpClient,httpGet);
        }
        catch (IOException e){
            System.err.println(e.getMessage());
            return Optional.empty();
        }
    }

    private Optional<String> response(CloseableHttpClient client,HttpGet get){
        try(CloseableHttpResponse response1 = client.execute(get)){
            return Optional.of(EntityUtils.toString(response1.getEntity()));
        }
        catch (IOException e){
            System.err.println(e.getMessage());
            return Optional.empty();
        }
    }
}
