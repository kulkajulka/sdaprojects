package julia.code.weatherApp;

import org.apache.http.client.utils.URIBuilder;

public class CityQuery implements Query{
    private final String city;
    private final String code;

    public CityQuery(String cityCode) {
        if (cityCode.contains(",")){
            this.city = cityCode.substring(0,cityCode.indexOf(","));
            this.code = cityCode.substring(cityCode.indexOf(","));
        } else {
            this.city = cityCode;
            this.code = null;
        }

    }

    @Override
    public String toString() {
//        return city + (code != null ? "," + code : "");

        StringBuilder sb = new StringBuilder();
        sb.append(city);
        if (code != null) {
            sb.append(',').append(code);
        }
        return sb.toString();
    }

    @Override
    public void applyToURIBuilder(URIBuilder uri) {
        uri.setParameter("q", this.toString());
    }
}
