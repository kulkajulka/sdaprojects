package julia.code.weatherApp;

import org.apache.http.client.utils.URIBuilder;

public class GeoQuery implements Query {
    private final double lat;
    private final double lon;
    private final String paramLat = "lat";
    private final String paramLon = "lon";

    public GeoQuery(double lat,double lon){
        this.lat = lat;
        this.lon = lon;
    }
    public String latToString(){
        return Double.toString(lat);
    }
    public String lonToString(){
        return Double.toString(lon);
    }

    public String getParamLat() {
        return paramLat;
    }

    public String getParamLon() {
        return paramLon;
    }

    @Override
    public void applyToURIBuilder(URIBuilder uri) {
        uri.setParameter(getParamLat(),latToString()).setParameter(getParamLon(),lonToString());
    }
}
