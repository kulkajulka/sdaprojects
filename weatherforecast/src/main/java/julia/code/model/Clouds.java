
package julia.code.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
@Data
public class Clouds {

    @SerializedName("all")
    @Expose
    private Integer all;


}
