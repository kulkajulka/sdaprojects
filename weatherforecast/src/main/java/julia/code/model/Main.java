
package julia.code.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
@Data
public class Main {

    @SerializedName("temp")
    @Expose
    private Double temp;
    @SerializedName("pressure")
    @Expose
    private Double pressure;
    @SerializedName("humidity")
    @Expose
    private Double humidity;
    @SerializedName("temp_min")  // tego nie wolno zmieniać
    @Expose
    private Double tempMin; // tutaj mogę nadawać własne nazwy
    @SerializedName("temp_max")
    @Expose
    private Double tempMax;


}
