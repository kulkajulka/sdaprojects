package sda.code.weatherapp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.asynchttpclient.*;
import sda.code.weatherapp.model.WeatherModel;

import java.util.concurrent.CompletableFuture;

import static sda.code.weatherapp.Constants.API_KEY;
import static sda.code.weatherapp.Constants.BASE_URL;

public class MainNiezly {
    public static void main(String[] args) {
        System.out.println("Pogoda");

        GeoQuery geoQuery = new GeoQuery(35.011667, 135.768333);

        while (true) {
            new MainNiezly(sharedClient).showWeather(BASE_URL, API_KEY, geoQuery);
            new MainNiezly(sharedClient).showWeather(BASE_URL, API_KEY, new CityQuery("Lodz"));
            new MainNiezly(sharedClient).showWeather(BASE_URL, API_KEY, new CityQuery("Pabianice"));

            śpij3sekundy();
        }
    }

    // Ulepszona wersja "zła" - współdzielony klient jest przekazywany jako parametr
    // do konstruktora klasy, którą będziemy wywoływać zapytanie - unikamy szpiegów.
    //
    // Jest to kanoniczne podejście do przekazywania zależności w programowaniu obiektowym.
    // Można podejść lepiej, jeśli użyje się przekazywanych funkcji, gdzie "lepiej" jest
    // rozumiane jako "mniej mocków".
    //
    // Komentarz do podejścia: jeśli zmieszać podejścia z MainNiezły i MainLepszy, czyli przekazać implementację przez
    // konstruktor (jako Function<> albo instancję własnego interfejsu), to moglibyśmy osiągnąć stan doskonałości
    public MainNiezly(AsyncHttpClient sharedClient) {
        client = sharedClient;
    }

    private AsyncHttpClient client;
    private final static AsyncHttpClient sharedClient = new DefaultAsyncHttpClient();

    CompletableFuture<Void> showWeather(String baseUrl, String apiKey, Query query) {

        return buildUri(baseUrl, apiKey, query)
                .thenComposeAsync(this::requestForecastJson)
                .thenApplyAsync(this::parseForecastJson)
                .thenAcceptAsync(this::printForecast)
                .exceptionally(t -> {
                    System.err.println("BŁĄD: " + t.getMessage());
                    return null;
                });
    }

    private WeatherModel parseForecastJson(String forecastJson) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.fromJson(forecastJson, WeatherModel.class);
    }

    private void printForecast(WeatherModel forecast) {
        System.out.print(forecast.getName());
        System.out.println(", " + forecast.getSys().getCountry());

        forecast.getWeather().forEach(w -> System.out.println(w.getDescription()));

        System.out.println(forecast.getMain().getTemp() + "℃");
    }

    private CompletableFuture<Request> buildUri(String baseUrl, String apiKey, Query query) {
        RequestBuilder builder = new RequestBuilder()
                .setUrl(baseUrl)
                .addQueryParam("appid", apiKey)
                .addQueryParam("units", "metric")
                .addQueryParam("lang", "pl");
        query.applyToBuilder(builder);
        return CompletableFuture.completedFuture(builder.build());
    }

    private CompletableFuture<String> requestForecastJson(Request request) {
        ListenableFuture<Response> listenable = client.executeRequest(request);
        CompletableFuture<Response> completable = listenable.toCompletableFuture();

        return completable
                .thenComposeAsync(response -> {
                    if (response.getStatusCode() == 200) {
                        return CompletableFuture.completedFuture(response);
                    } else {
                        throw new IllegalArgumentException("Nie działa - " + response.getStatusCode());
                    }
                })
                .thenApplyAsync(Response::getResponseBody);
    }

    private static void śpij3sekundy() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
