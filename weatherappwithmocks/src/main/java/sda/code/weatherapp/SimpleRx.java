package sda.code.weatherapp;

import rx.Observable;
import rx.Observer;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

public class SimpleRx {
    public static void main(String[] args) throws InterruptedException {
        // observable, który wyemituje zadane elementy i zakończy się
        Observable<Integer> numbers = Observable.just(1, 2, 3, 10, 12)
                .filter(i -> i < 10)
                .map(i -> i * 2);
        numbers.observeOn(Schedulers.newThread()).subscribe(i -> System.err.println("Ha! " + i));
        numbers.observeOn(Schedulers.newThread()).subscribe(new Observer<Integer>() {
            @Override
            public void onCompleted() {
                System.out.println("Skończyłem");
            }

            @Override
            public void onError(Throwable e) {
                System.err.println("Błąd: " + e.getMessage());
            }

            @Override
            public void onNext(Integer integer) {
                System.out.println("Liczba: " + integer);
            }
        });

        // tworzymy "temat" do publikacji, w jednym wątku do niego wkładamy elementy
        // a za pomocą RxJavy nasłuchiwać na
        final PublishSubject<Integer> subject = PublishSubject.create();
        subject.asObservable()
//                .observeOn(Schedulers.computation())
                .doOnNext(i -> System.out.println(Thread.currentThread().getName() + ": " + i))
//                .observeOn(Schedul/media/julia/hdd/SDAProjects/weatherforecasters.newThread())
                .subscribe(i -> System.out.println(Thread.currentThread().getName() + ": " + i));

        // nasz wątek publikujący
        new Thread(() -> {
            for (int i = 0; i < 100; ++i) {
                System.out.println(Thread.currentThread().getName() + ": Pushing " + i);
                subject.onNext(i);
            }
        }).start();

        Thread.sleep(2000);
    }
}
