package sda.code.weatherapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sda.code.weatherapp.model.WeatherModel;
import sda.code.weatherapp.retrofit.CityQuery;
import sda.code.weatherapp.retrofit.GeoQuery;
import sda.code.weatherapp.retrofit.OpenWeather;
import sda.code.weatherapp.retrofit.QueryBuilder;

import java.io.IOException;
import java.util.Optional;

import static sda.code.weatherapp.Constants.API_KEY;

public class MainRetrofitWithLogging {
    private static final Logger LOG = LoggerFactory.getLogger(MainRetrofitWithLogging.class);

    public static void main(String[] args) {
        LOG.info("Pogoda");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.OW_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        OpenWeather api = retrofit.create(OpenWeather.class);

        GeoQuery geoQuery = new GeoQuery(35.011667, 135.768333);

        while (true) {
            try {
                sync(new QueryBuilder(api, API_KEY).withQuery(geoQuery).build())
                        .ifPresent(MainRetrofitWithLogging::printForecast);
                sync(new QueryBuilder(api, API_KEY).withQuery(new CityQuery("Lodz")).build())
                        .ifPresent(MainRetrofitWithLogging::printForecast);
                sync(new QueryBuilder(api, API_KEY)
                        .withQuery(new CityQuery("Pabianice"))
                        .withLangCode("fr")
                        .withUnits("imperial")
                        .build())
                        .ifPresent(MainRetrofitWithLogging::printForecast);
            } catch (IOException e) {
                LOG.error("Failed to download weather info", e);
            }

            beIdle(3);
        }
    }

    private static <T> Optional<T> sync(Call<T> call) throws IOException {
        Response<T> response = call.execute();
        if (response.isSuccessful()) {
            return Optional.ofNullable(response.body());
        } else {
            if (response.errorBody() != null) {
                LOG.error(response.errorBody().string());
            } else {
                LOG.error("Empty error response body, but failed to download");
            }
            return Optional.empty();
        }
    }

    private static void printForecast(WeatherModel forecast) {
        LOG.info("{}, {}", forecast.getName(), forecast.getSys().getCountry());

        forecast.getWeather().forEach(w -> LOG.info(w.getDescription()));

        LOG.info("{}℃", forecast.getMain().getTemp());
    }

    private static void beIdle(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
