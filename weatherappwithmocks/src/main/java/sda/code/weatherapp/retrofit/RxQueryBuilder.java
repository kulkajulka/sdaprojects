package sda.code.weatherapp.retrofit;

import retrofit2.Call;
import rx.Observable;
import sda.code.weatherapp.model.WeatherModel;

import static java.util.Objects.requireNonNull;

public class RxQueryBuilder {
    private final RxOpenWeather api;
    private final String apiKey;
    private GeoQuery geoQuery;
    private CityQuery cityQuery;
    private String units = "metric";
    private String langCode = "pl";

    public RxQueryBuilder(RxOpenWeather api, String apiKey) {
        this.api = requireNonNull(api);
        this.apiKey = requireNonNull(apiKey);
    }

    public RxQueryBuilder withQuery(GeoQuery geoQuery) {
        this.geoQuery = requireNonNull(geoQuery);
        return this;
    }

    public RxQueryBuilder withQuery(CityQuery cityQuery) {
        this.cityQuery = requireNonNull(cityQuery);
        return this;
    }

    public RxQueryBuilder withUnits(String units) {
        this.units = requireNonNull(units);
        return this;
    }

    public RxQueryBuilder withLangCode(String langCode) {
        this.langCode = requireNonNull(langCode);
        return this;
    }

    void validate() {
        if (cityQuery != null && geoQuery != null) {
            throw new IllegalStateException("Cannot pass both queries at once");
        }
        if (cityQuery == null && geoQuery == null) {
            throw new IllegalStateException("Must pass at least one query");
        }
    }

    public Observable<WeatherModel> build() {
        validate();
        return api.weather(
                apiKey,
                cityQuery != null ? cityQuery.toString() : null,
                geoQuery != null ? geoQuery.getLat() : null,
                geoQuery != null ? geoQuery.getLon() : null,
                units,
                langCode
        );
    }
}
