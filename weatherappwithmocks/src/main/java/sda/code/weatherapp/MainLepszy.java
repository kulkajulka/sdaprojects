package sda.code.weatherapp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.asynchttpclient.*;
import sda.code.weatherapp.model.WeatherModel;

import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

import static sda.code.weatherapp.Constants.API_KEY;
import static sda.code.weatherapp.Constants.BASE_URL;

public class MainLepszy {
    public static void main(String[] args) {
        System.out.println("Pogoda");

        GeoQuery geoQuery = new GeoQuery(35.011667, 135.768333);

        while (true) {
            showWeather(BASE_URL, API_KEY, geoQuery);
            showWeather(BASE_URL, API_KEY, new CityQuery("Lodz"), request -> client.executeRequest(request).toCompletableFuture());
            showWeather(BASE_URL, API_KEY, new CityQuery("Pabianice"));

            śpij3sekundy();
        }
    }

    // Wersja "trochę lepsza" od pozostałych, bo potrzebuje 3x mniej atrap, żeby dało się ją przetestować.
    // Zastępujemy funkcję z 4. argumentu naszą własną, która zwraca nam spreparowaną odpowiedź.
    // Dzięki temu nie musimy mockować całej interakcji z AsyncHttpClient - nasza nowa funkcja leży
    // na punktach styku między kodem naszej aplikacji a interakcją z zewnątrznym bytem, jakim jest klient.
    // Wciąż potrzebujemy mocka obiektu odpowiedzi, ponieważ interfejs Response jest bardzo rozbudowany,
    // a nas interesują tylko dwie metody, nad którymi chcemy mieć kontrolę.
    //
    // Komentarz do podejścia: jeśli zmieszać podejścia z MainNiezły i MainLepszy, czyli przekazać implementację przez
    // konstruktor (jako Function<> albo instancję własnego interfejsu), to moglibyśmy osiągnąć stan doskonałości
    private static final AsyncHttpClient client = new DefaultAsyncHttpClient();

    private static CompletableFuture<Void> showWeather(
            String baseUrl,
            String apiKey,
            Query query) {
        return showWeather(baseUrl, apiKey, query,
                request -> client.executeRequest(request).toCompletableFuture());
    }

    static CompletableFuture<Void> showWeather(
            String baseUrl,
            String apiKey,
            Query query,
            Function<Request, CompletableFuture<Response>> execRequest) {

        return buildUri(baseUrl, apiKey, query)
                .thenComposeAsync(uri -> MainLepszy.requestForecastJson(uri, execRequest))
                .thenApplyAsync(MainLepszy::parseForecastJson)
                .thenAcceptAsync(MainLepszy::printForecast)
                .exceptionally(t -> {
                    System.err.println("BŁĄD: " + t.getMessage());
                    return null;
                });
    }

    private static WeatherModel parseForecastJson(String forecastJson) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.fromJson(forecastJson, WeatherModel.class);
    }

    private static void printForecast(WeatherModel forecast) {
        System.out.print(forecast.getName());
        System.out.println(", " + forecast.getSys().getCountry());

        forecast.getWeather().forEach(w -> System.out.println(w.getDescription()));

        System.out.println(forecast.getMain().getTemp() + "℃");
    }

    private static CompletableFuture<Request> buildUri(String baseUrl, String apiKey, Query query) {
        RequestBuilder builder = new RequestBuilder()
                .setUrl(baseUrl)
                .addQueryParam("appid", apiKey)
                .addQueryParam("units", "metric")
                .addQueryParam("lang", "pl");
        query.applyToBuilder(builder);
        return CompletableFuture.completedFuture(builder.build());
    }

    private static CompletableFuture<String> requestForecastJson(
            Request request,
            Function<Request, CompletableFuture<Response>> execRequest) {

        return execRequest.apply(request)
                .thenComposeAsync(response -> {
                    if (response.getStatusCode() == 200) {
                        return CompletableFuture.completedFuture(response);
                    } else {
                        CompletableFuture<Response> fail = new CompletableFuture<>();
                        fail.obtrudeException(new IllegalArgumentException("Nie działa - " + response.getStatusCode()));
                        return fail;
                    }
                })
                .thenApplyAsync(Response::getResponseBody);
    }

    private static void śpij3sekundy() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
