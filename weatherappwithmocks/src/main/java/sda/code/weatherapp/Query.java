package sda.code.weatherapp;

import org.asynchttpclient.RequestBuilder;

public interface Query {
    void applyToBuilder(RequestBuilder builder);
}
