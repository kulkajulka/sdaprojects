package sda.code.weatherapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sda.code.weatherapp.model.WeatherModel;
import sda.code.weatherapp.retrofit.CityQuery;
import sda.code.weatherapp.retrofit.GeoQuery;
import sda.code.weatherapp.retrofit.OpenWeather;
import sda.code.weatherapp.retrofit.QueryBuilder;

import java.io.IOException;
import java.util.function.Consumer;

import static sda.code.weatherapp.Constants.API_KEY;

public class AsyncMainRetrofitWithLogging {
    private static final Logger LOG =
            LoggerFactory.getLogger(AsyncMainRetrofitWithLogging.class);

    public static void main(String[] args) {
        LOG.info("Pogoda");

        LOG.debug("Tworzę zwykłą fabrykę");
        Retrofit retrofitServiceFactory = new Retrofit.Builder()
                .baseUrl(Constants.OW_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        LOG.debug("Tworzę zepsutą fabrykę");
        Retrofit faultyServiceFactory = new Retrofit.Builder()
                .baseUrl(Constants.OW_BASE + ".abc")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        OpenWeather api = retrofitServiceFactory.create(OpenWeather.class);
        OpenWeather badApi = faultyServiceFactory.create(OpenWeather.class);

        GeoQuery geoQuery = new GeoQuery(35.011667, 135.768333);

        LOG.debug("Zaczynam pętlę");
        while (true) {
            LOG.debug("{}", retrofitServiceFactory);

            async(new QueryBuilder(api, API_KEY).withQuery(geoQuery).build(),
                    AsyncMainRetrofitWithLogging::printForecast);
            async(new QueryBuilder(api, API_KEY + "asda").withQuery(new CityQuery("Lodz")).build(),
                    AsyncMainRetrofitWithLogging::printForecast);
            async(new QueryBuilder(api, API_KEY)
                            .withQuery(new CityQuery("Pabianice"))
                            .withLangCode("fr")
                            .withUnits("imperial")
                            .build(),
                    AsyncMainRetrofitWithLogging::printForecast);
            async(new QueryBuilder(badApi, API_KEY).withQuery(new CityQuery("Lodz")).build(),
                    AsyncMainRetrofitWithLogging::printForecast);

            beIdle(3);
        }
    }

    private static <T> void async(Call<T> call, Consumer<T> consumer) {
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                if (response.isSuccessful()) {
                    consumer.accept(response.body());
                } else {
                    try {
                        LOG.warn("Error: {}", response.errorBody().string());
                    } catch (IOException e) {
                        LOG.error("Failed to read the error body", e);
                    }
                }
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                LOG.error("Failed to retrieve content", t);
            }
        });
    }

    private static void printForecast(WeatherModel forecast) {
        LOG.info("{}, {}", forecast.getName(), forecast.getSys().getCountry());

        forecast.getWeather().forEach(w -> LOG.info("{}", w.getDescription()));

        LOG.info("{}℃", forecast.getMain().getTemp());
    }

    private static void beIdle(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
