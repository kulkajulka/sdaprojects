package sda.code.weatherapp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.asynchttpclient.*;
import sda.code.weatherapp.model.WeatherModel;

import java.util.concurrent.CompletableFuture;

import static sda.code.weatherapp.Constants.API_KEY;
import static sda.code.weatherapp.Constants.BASE_URL;

public class MainBardzoZly {
    public static void main(String[] args) {
        System.out.println("Pogoda");

        GeoQuery geoQuery = new GeoQuery(35.011667, 135.768333);

        while (true) {
            showWeather(BASE_URL, API_KEY, geoQuery);
            showWeather(BASE_URL, API_KEY, new CityQuery("Lodz"));
            showWeather(BASE_URL, API_KEY, new CityQuery("Pabianice"));

            śpij3sekundy();
        }
    }

    // Tutaj jest ten najgorszy element - żeby podłożyć atrapę pod clienta, trzeba było
    // zdjąć modyfikator "final".
    static AsyncHttpClient client = new DefaultAsyncHttpClient();

    static CompletableFuture<Void> showWeather(String baseUrl, String apiKey, Query query) {

        return buildUri(baseUrl, apiKey, query)
                .thenComposeAsync(MainBardzoZly::requestForecastJson)
                .thenApplyAsync(MainBardzoZly::parseForecastJson)
                .thenAcceptAsync(MainBardzoZly::printForecast)
                .exceptionally(t -> {
                    System.err.println("BŁĄD: " + t.getMessage());
                    return null;
                });
    }

    private static WeatherModel parseForecastJson(String forecastJson) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.fromJson(forecastJson, WeatherModel.class);
    }

    private static void printForecast(WeatherModel forecast) {
        System.out.print(forecast.getName());
        System.out.println(", " + forecast.getSys().getCountry());

        forecast.getWeather().forEach(w -> System.out.println(w.getDescription()));

        System.out.println(forecast.getMain().getTemp() + "℃");
    }

    private static CompletableFuture<Request> buildUri(String baseUrl, String apiKey, Query query) {
        RequestBuilder builder = new RequestBuilder()
                .setUrl(baseUrl)
                .addQueryParam("appid", apiKey)
                .addQueryParam("units", "metric")
                .addQueryParam("lang", "pl");
        query.applyToBuilder(builder);
        return CompletableFuture.completedFuture(builder.build());
    }

    private static CompletableFuture<String> requestForecastJson(Request request) {
        ListenableFuture<Response> listenable = client.executeRequest(request);
        CompletableFuture<Response> completable = listenable.toCompletableFuture();

        return completable
                .thenComposeAsync(response -> {
                    if (response.getStatusCode() == 200) {
                        return CompletableFuture.completedFuture(response);
                    } else {
                        throw new IllegalArgumentException("Nie działa - " + response.getStatusCode());
                    }
                })
                .thenApplyAsync(Response::getResponseBody);
    }

    private static void śpij3sekundy() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
