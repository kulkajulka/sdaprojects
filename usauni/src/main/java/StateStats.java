public class StateStats {
    public String name;
    public int count;
    public double min;
    public double max;
    public double sum;
    public StateStats(String name){
        this.name = name;
        this.count = 0;
        this.min = 0;
        this.max = 0;
        this.sum = 0;
    }
    public StateStats(String name, int count, double min,double max, double sum){
        this.name = name;
        this.count = count;
        this.min = min;
        this.max = max;
        this.sum = sum;
    }
    public void add(double num){
        count++;
        if(max < num){
            max = num;
        }
        if (min > num){
            min = num;
        }
        sum+=num;
    }
}
