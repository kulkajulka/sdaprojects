import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Csv {
    public static void main(String[] args) throws IOException {
        File csv = Paths.get(System.getProperty("user.home"),"Desktop","MERGED2012_PP.csv").toFile();
        File summary = Paths.get(System.getProperty("user.home"),"Desktop","MERGED2012_PP_SUMMARY.csv").toFile();

        try(Reader reader = new FileReader(csv);
            Writer writer = new FileWriter(summary)){
            summarize(reader,writer);
        }
    }

    private static void summarize(Reader reader, Writer writer) throws IOException {
        Map<String,StateStats> mapa = new LinkedHashMap<>();
        try(CSVParser parser = new CSVParser(reader, CSVFormat.RFC4180.withFirstRecordAsHeader())){
                for (CSVRecord row : parser) {
                String state = row.get("STABBR");
                Double num;
                try{
                    num = Double.parseDouble(row.get("SAT_AVG_ALL"));
                } catch (NumberFormatException e){
                    continue;
                }
                StateStats StateSoFar = mapa.getOrDefault(state,new StateStats(state));
                StateSoFar.add(num);
                mapa.put(state,StateSoFar);
                /*if(mapa.containsKey(state)){
                    mapa.get(state).add(num);
                }
                else {
                    mapa.put(state,new StateStats(state,1,num,num,num));
                }*/
            }
        }
        try (CSVPrinter printer = new CSVPrinter(writer,CSVFormat.RFC4180.withHeader("Group", "Size", "Avg. Value", "Min. Value", "Max. Value"))){
            mapa.entrySet().stream()
                    .forEach(g -> {
                        try {
                            printer.printRecord(g.getValue().name, g.getValue().count,
                                    g.getValue().sum / g.getValue().count, g.getValue().min, g.getValue().max);
                        } catch (IOException e) {
                            /* przerzucam wyjątek, opakowując go w niesprawdzany wyjątek*/
                             throw new RuntimeException(e);    //tak nie robimy (za mało) ->> e.printStackTrace();
                        }
                    });
        }
    }
}
