<%@ page import="julia.codes.CalcParser" %>
<%@ page import="julia.codes.Computation" %>
<%@ page import="julia.codes.Calculator" %><%--
  Created by IntelliJ IDEA.
  User: julia
  Date: 30.09.17
  Time: 14:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Basic Calculator</title>
</head>
<body>
<h1 style="color: crimson; background-color: antiquewhite; border: dashed; text-align: center; font-family: Ubuntu">
    Welcome! This is Basic Calculator Julia has created for you!
</h1>
<p style="text-align: center; font-family: Ubuntu">In order to calculate something, you need to send query. Please stick to this pattern: </p>
<p style="text-align: center; font-family: monospace">localhost:8080/calc.jsp?nr1=1&comp=+&nr2=2</p>
<p style="text-align: center; font-family: Ubuntu">Available computations: '+', '-', '/', '*', 'pow'.</p>
<p style="text-align: center; font-family: Ubuntu">
    <%
        String params = request.getQueryString();
        if (params == null)
        {
            out.println("Please create query with appropriate parameters!");
        }
        else
        {
            CalcParser calcParser = new CalcParser(params);
            int status = calcParser.getStatus();
            if (status == 200)
            {
                Computation comp = calcParser.getComputation();
                double nr1 = calcParser.getFirstNumber();
                double nr2 = calcParser.getSecondNumber();
                out.println("Result of "+comp+" of numbers: ");
                out.println("<br/>");
                out.println(nr1+" and "+nr2);
                out.println("<br/>");
                out.println("is: "+ Calculator.compute(comp,nr1,nr2));
            }
            else
            {
                response.sendError(status, calcParser.getMessage());
            }
        }
    %>
</p>
</body>
</html>
