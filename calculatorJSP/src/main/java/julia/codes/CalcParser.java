package julia.codes;

import static julia.codes.Computation.*;

public class CalcParser {
    private Computation comp;
    private double firstNumber;
    private double secondNumber;
    private String message;
    private int status;

    public CalcParser(String params){
        status = parseAndValidate(params);
    }

    public Computation getComputation() {
        return comp;
    }

    public double getFirstNumber() {
        return firstNumber;
    }

    public double getSecondNumber() {
        return secondNumber;
    }

    private int parseAndValidate(String params){

        String[] splittedParams = params.split("&");
        if (splittedParams.length != 3)
        {
            message = "Wrong number of parameters!";
            return 400;
        }
        try
        {
            firstNumber = Double.parseDouble(splittedParams[0].split("=")[1]);
            secondNumber = Double.parseDouble(splittedParams[2].split("=")[1]);
        }
        catch (NumberFormatException ex)
        {
            message = "Wrong number format";
            return 400;
        }
        String tempComp = splittedParams[1].split("=")[1];
        switch (tempComp){
            case "+":
                comp = ADDITION;
                break;
            case "-":
                comp = SUBTRACTION;
                break;
            case "/":
                if (secondNumber == 0)
                {
                    message = "Second number cannot be 0!";
                    return 400;
                }
                comp = DIVISION;
                break;
            case "*":
                comp = MULTIPLICATION;
                break;
            case "pow":
                comp = EXPONENTIATION;
                break;
            default:
                message = "Unknown computation";
                return 400;
        }
        return 200;
    }

    public String getMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }
}
