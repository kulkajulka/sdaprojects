package julia.codes;

public class Calculator {
    public static Double compute(Computation comp, double nr1, double nr2) {
        switch (comp){
            case ADDITION:
                return nr1+nr2;
            case DIVISION:
                return nr1/nr2;
            case SUBTRACTION:
                return nr1-nr2;
            case EXPONENTIATION:
                return Math.pow(nr1,nr2);
            case MULTIPLICATION:
                return nr1*nr2;
        }
        return 0.0;
    }
}
