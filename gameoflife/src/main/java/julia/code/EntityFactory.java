package julia.code;

public interface EntityFactory {
    Entity createEntity();
    Entity createEntity(char ch,boolean state);
}
