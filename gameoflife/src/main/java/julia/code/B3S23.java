package julia.code;

import julia.code.squirrels.Squirrel;
import julia.code.squirrels.SquirrelFactory;

public class B3S23 implements Generational{

    private final EntityFactory factory;

    public B3S23(EntityFactory factory){
        this.factory = factory;
    }

    public void initializeGeneration(Entity[][] enti) {
        int ver = enti.length;
        int hor = enti[0].length;
        for (int i = 0; i < ver; i++) {
            for (int j = 0; j < hor; j++) {
                switch((i+j) % 3){
                    case 0:
                        enti[i][j] = factory.createEntity('%',Math.random()>0.85);
                        break;
                    case 1:
                        enti[i][j] = factory.createEntity('$',Math.random()>0.85);
                        break;
                    case 2:
                        enti[i][j] = factory.createEntity();
                        break;
                }
            }
        }
    }

    public Entity[][] nextGeneration(Entity[][] enti) {
        int ver = enti.length;
        int hor = enti[0].length;
        Entity[][] newState = new Entity[ver][hor];
        /*for (int i = 0; i < ver; i++) {
            for (int j = 0; j < hor; j++) {
                newState[i][j] = Entity(enti[i][j]);
            }
        }*/
        /*for (int i = 0; i < hor; i++) {
            newState[0][i].kill();
        }
        for (int i = 0; i < hor; i++) {
            newState[ver-1][i].kill();
        }
        for (int i = 0; i < ver; i++) {
            newState[i][0].kill();
        }
        for (int i = 0; i < ver; i++) {
            newState[i][hor-1].kill();
        }*/
        for (int i = 0; i < ver; i++) {
            for (int j = 0; j < hor; j++) {
                int neighbours = 0;
                if (i-1 >=0 && j-1 >= 0)
                    if (enti[i-1][j-1].isAlive()) neighbours++;
                if (i-1 >= 0)
                    if (enti[i-1][j].isAlive()) neighbours++;
                if (i-1 >= 0 && j+1 < hor)
                    if (enti[i-1][j+1].isAlive()) neighbours++;
                if (j-1 >= 0)
                    if (enti[i][j-1].isAlive()) neighbours++;
                if (j+1 < hor)
                    if (enti[i][j+1].isAlive()) neighbours++;
                if (i+1 < ver && j-1 >= 0)
                    if (enti[i+1][j-1].isAlive()) neighbours++;
                if (i+1 < ver)
                    if (enti[i+1][j].isAlive()) neighbours++;
                if (i+1 < ver && j+1 < hor)
                    if (enti[i+1][j+1].isAlive()) neighbours++;

                if (enti[i][j].isAlive()) {
                    switch (neighbours) {
                        case 0:
                        case 1:
                            newState[i][j] = enti[i][j].die();
                            break;
                        case 2:
                        case 3:
                            newState[i][j] = enti[i][j].live();
                            break;
                        default:
                            newState[i][j] = enti[i][j].die();
                    }
                }
                else {
                    if (neighbours == 3) newState[i][j] = enti[i][j].live();
                    else newState[i][j] = enti[i][j].die();
                }
            }
        }
        return newState;
    }
}
