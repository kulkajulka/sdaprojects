package julia.code;

public class World {
    private final int vertical;
    private final int horizontal;
    private Generational generational;
    public Entity[][] entities;

    public World(Generational generational,int ver,int hor){
        entities = new Entity[ver][hor];
        this.generational = generational;
        vertical = ver;
        horizontal = hor;
    }

    public void beBorn(){
        generational.initializeGeneration(entities);
    }

    public void proceed(){
        entities = generational.nextGeneration(entities);
    }

    public int getVertical(){
        return vertical;
    }

    public int getHorizontal(){
        return horizontal;
    }

}
