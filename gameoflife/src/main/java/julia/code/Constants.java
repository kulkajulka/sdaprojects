package julia.code;

public class Constants {
    public static final int VSIZE = 30;
    public static final int HSIZE = 50;
    public static final int INTERVAL = 1000;
}
