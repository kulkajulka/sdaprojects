package julia.code;

public interface Entity {
    boolean isAlive();
    Entity die();
    Entity live();
    char getChar();
}

