package julia.code.squirrels;

import julia.code.Entity;

public class Squirrel implements Entity {
    private final boolean state;
    private final char character;

    Squirrel(char ch, boolean initialState){   // package visible
        this.state = initialState;
        this.character = ch;
    }
    public boolean isAlive() {
        return state;
    }

    public Entity die() {
        return new Squirrel(character, false);
    }

    public Entity live() {
        return new Squirrel(character, true);
    }

    public char getChar() {
        return character;
    }
}
