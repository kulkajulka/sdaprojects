package julia.code.squirrels;

import julia.code.Entity;
import julia.code.EntityFactory;

public class SquirrelFactory implements EntityFactory {
    private final boolean initialState;
    private final char character;

    public SquirrelFactory(){
        initialState = true;
        character ='@';
    }

    public SquirrelFactory(boolean initialState,char ch){
        this.initialState = initialState;
        character = ch;
    }

    public Entity createEntity(){
        return new Squirrel(character,initialState);
    }

    @Override
    public Entity createEntity(char ch, boolean state) {
        return new Squirrel(ch,state);
    }
}
