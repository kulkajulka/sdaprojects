package julia.code;

public interface Generational {
    void initializeGeneration(Entity[][] enti);
    Entity[][] nextGeneration(Entity[][] enti);
}
