package julia.code;

// Ustal rozmiary świata gry
// Losowanie stanu początkowego, dla każdej komórki
// Pętla świata (nieskończona albo z dużą ilością powtórzeń, >100)
// - Wyświetl stan świata na konsoli
//   (użyj spacji do reprezentowania pustej komórki i '@' albo '#' dla pełnej
// - Stwórz nowy, pusty stan dla następnego kroku
// - Dla każdej komórki z poprzedniego kroku sprawdź sąsiedztwo
//   (8 sąsiadów, komórki "poza światem" traktuj jako puste)
// - Na podstawie sąsiedztwa zdecyduj, czy dana lokalizacja w nowym kroku będzie pusta czy pełna
//   (klasyczne zasady to: B3/S23)
// - Ustaw nowy stan świata jako bieżący

// Co dalej?
// - Logikę decydującą o nowym stanie wynieś do osobnej funkcji, stwórz drugą metodę,
//   która będzie implementować inny zestaw reguł (np. B36/S23, B3678/S34678)
// - Dodawaj więcej abstrakcji... (zobacz aplikację w pakiecie answers, żeby zobaczyć
//   wersję z już zbyt wielką ilością abstrakcji)

import julia.code.squirrels.SquirrelFactory;
import julia.code.squirrels.Squirrel;

public class GameOfLife {

    private World world;
    private int interval;


    public GameOfLife(World world,int interval){
        this.world = world;
        this.interval = interval;
    }

    public void printStateOfWorld(){
        for (int i = 0; i < world.getVertical(); i++) {
            for (int j = 0; j < world.getHorizontal(); j++) {
                if(world.entities[i][j].isAlive())
                    System.out.print(world.entities[i][j].getChar());
                else System.out.print(' ');
            }
            System.out.println();
        }
        System.out.println();
    }

    public void start() throws InterruptedException {
        world.beBorn();
        printStateOfWorld();
        while(true){
            world.proceed();
            printStateOfWorld();
            Thread.sleep(interval);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Generational modelB3S23 = new B3S23(new SquirrelFactory());
        World world = new World(modelB3S23,Constants.VSIZE,Constants.HSIZE);
        GameOfLife gameOfLife = new GameOfLife(world,Constants.INTERVAL);
        gameOfLife.start();

    }
}
