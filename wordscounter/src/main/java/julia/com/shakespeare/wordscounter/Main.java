package julia.com.shakespeare.wordscounter;

import java.beans.IntrospectionException;
import java.io.*;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws IOException {
        /*System.getProperty zwraca ścięzke, ustawenia systemowe*/
        System.out.println(System.getProperty("user.home"));
        /*obsługa ścieżek do plików*/
        File source = Paths
                .get(System.getProperty("user.home"),"Desktop","Shakespeare.txt").toFile();
        /* try with resources, nie musimy zamykać strumienia do pliku ins.close()*/
        try(Scanner ins = new Scanner(source).useDelimiter("\\W+")){
            printMapDescending(sortByValue(doYourJob(ins)));
        }
        /* drukujemy całą ścieżkę */
        System.out.println(source.getAbsolutePath());
    }
    /* statuczna metoda wczytujaca dane z pliku */
    private static HashMap<String,Integer> doYourJob(Scanner in){
        /* nowy pojemnik na słowa z kluczami */
        HashMap<String,Integer> counter = new HashMap<>();
        //Pattern p = Pattern.compile("\\W+"); <-- można tego użyć by nie powtarzać .matches(regex)
        while(in.hasNext()){
            String word = in.next().toLowerCase();
            //if(p.matcher(word).matches()) continue;
            //int num = counter.getOrDefault(word,0);
            if (counter.containsKey(word)) {
                counter.put(word,counter.get(word) + 1);
            }
            else {
                counter.put(word,1);
            }
        }
        return counter;
    }
    /* metoda sortujaca hashmape, zwraca Map*/
    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        return map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (e1, e2) -> e1,
                        LinkedHashMap::new
                ));
    }
    /* metoda drukuja 20 najczesciej wystepujacych słów */
    public static void printMapDescending(Map<String,Integer> map){
        map.entrySet().stream().limit(20).forEach(System.out::println);
    }
}
