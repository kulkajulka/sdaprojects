package pl.lodz.sda;

import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.UpdateResult;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.json.JsonWriterSettings;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.bson.json.JsonWriterSettings.builder;
import static org.junit.Assert.assertTrue;

public class MongoDBConnectorTest {
    public static String GRADES = "grades";
    private static final Logger LOGGER = Logger.getLogger(MongoDBConnectorTest.class);
    MongoDBConnector mongoDBConnector = new MongoDBConnector();

    @Test
    public void findDocuments() throws Exception {
        // given

        Document bson = new Document("student_id",new Document("$gt",100).append("$lt",110));
        Document studentId = new Document().append("student_id",1).append("_id",0).append("score",1);

        // when
        FindIterable<Document> documents = mongoDBConnector.findDocuments(GRADES,bson,studentId);

        // then
        MongoCursor<Document> iterator = documents.iterator();

        JsonWriterSettings.Builder withIndents = builder().indent(true);

        while (iterator.hasNext()){
            Document document = iterator.next();
            assertTrue(document.containsKey("student_id"));
            assertTrue((Integer)document.get("student_id") > 100);
            LOGGER.info("Nasz dokument: "+document.toJson(withIndents.build()));

        }

    }

    @Test
    public void findDocumentsWithFiltersMongoAPI(){
        // given
        Bson studentId = Filters.gt("student_id",100);
        Bson typeExam = new Document("type","exam");
        Bson and = Filters.and(studentId,typeExam);

        Document studentIdP = new Document().append("student_id",1).append("_id",0).append("type",1);

        // when
        FindIterable<Document> documents = mongoDBConnector.findDocuments(GRADES,and,studentIdP);

        // then
        MongoCursor<Document> iterator = documents.iterator();

        JsonWriterSettings.Builder withIndents = builder().indent(true);

        while (iterator.hasNext()){
            Document document = iterator.next();
            assertTrue(document.containsKey("student_id"));
            assertTrue(document.containsKey("type"));
            assertTrue(((Integer)document.get("student_id")) > 100);
            assertTrue(StringUtils.equals(""+document.get("type"),"exam"));
            LOGGER.info("Nasz dokument: "+document.toJson(withIndents.build()));
        }
        iterator.close();

    }

    @Test
    public void findDocumentsWithFiltersMongoAPIandAverage(){
        // given
        List<Bson> list = new ArrayList<Bson>();
        Bson match = Aggregates.match(
                Filters.and(
                        Filters.gt("student_id",100),
                        Filters.lt("student_id",110)));
        Bson group = Aggregates.group("$student_id", Accumulators
                .avg("średnia","$score"));
        Bson sort = Aggregates.sort(new Document("_id", 1));
        list.add(match);
        list.add(group);
        list.add(sort);
        // when
        AggregateIterable<Document> documents = mongoDBConnector
                .findAndAggregate(GRADES,list);

        // then
        MongoCursor<Document> iterator = documents.iterator();

        JsonWriterSettings.Builder withIndents = builder().indent(true);

        while (iterator.hasNext()){
            Document document = iterator.next();
            assertTrue(document.containsKey("_id"));
            assertTrue(document.containsKey("średnia"));
            assertTrue(((Integer)document.get("_id")) > 100);
            assertTrue(((Integer)document.get("_id")) < 110);
            LOGGER.info("Nasz dokument: "+document.toJson(withIndents.build()));
        }
        iterator.close();

    }

    @Test
    public void findAndUpdateExamScores(){
        // given
        Bson match = Filters.and(
                        Filters.gt("student_id",100),
                        Filters.lt("student_id",120),
                        Filters.gt("score",10),
                        Filters.lt("score",20),
                        Filters.eq("type","exam"));

        Bson projection = new Document("student_id",1).append("_id",0).append("score",1).append("type",1);
        Bson update = Updates.inc("score",1);

        // when
        UpdateResult documents1 = mongoDBConnector
                .findAndUpdateDocuments(GRADES,match,update);
        FindIterable<Document> documents = mongoDBConnector
                .findDocuments(GRADES,match,projection);

        // then
        MongoCursor<Document> iterator = documents.iterator();

        JsonWriterSettings.Builder withIndents = builder().indent(true);

        while (iterator.hasNext()){
            Document document = iterator.next();
            assertTrue(document.containsKey("type"));
            assertTrue(document.containsKey("score"));
            assertTrue(document.containsKey("student_id"));
            assertTrue(((Integer)document.get("student_id")) > 100);
            assertTrue(((Integer)document.get("student_id")) < 120);
            LOGGER.info("Nasz dokument: "+document.toJson(withIndents.build()));
        }
        iterator.close();

        LOGGER.info("Liczba updateów: "+documents1.getModifiedCount());
    }
}