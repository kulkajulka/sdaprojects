package pl.lodz.sda;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.UpdateResult;
import org.apache.log4j.Logger;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MongoDBConnector implements MongoDBConnectorApi{
    final static Logger logger  = Logger.getLogger(MongoDBConnector.class);

    private static PropertiesLoader propertiesLoader = new PropertiesLoader();

    private MongoClient client;

    private void init(){
        try{
            propertiesLoader.init();
            prepareConfiguration();
        } catch (IOException e){
         logger.error(e);
        }
    }

    private void prepareConfiguration(){
        ServerAddress address = new ServerAddress(propertiesLoader.getHost(), propertiesLoader.getPort());
        List<MongoCredential> credentials = new ArrayList<MongoCredential>();
        MongoCredential credential = MongoCredential.createCredential(propertiesLoader.getUser(),
                propertiesLoader.getDatabase(), propertiesLoader.getPass().toCharArray());
        credentials.add(credential);
        client = new MongoClient(address, credentials);
    }

    private MongoDatabase connect(){
        init();
        return client.getDatabase(propertiesLoader.getDatabase());
    }

    public FindIterable<Document> findDocuments(String collectionName,
                                             Bson filter,
                                             Bson projection){
        MongoCollection<Document> collection = getMongoCollection(collectionName);
        return collection.find(filter).projection(projection);
    }

    public AggregateIterable<Document> findAndAggregate(String collectionName,
                                                                 List<Bson> pipeline){
        MongoCollection<Document> collection = getMongoCollection(collectionName);
        return collection.aggregate(pipeline);
    }

    private MongoCollection<Document> getMongoCollection(String collectionName) {
        MongoDatabase database = connect();
        return database.getCollection(collectionName);
    }

    public UpdateResult findAndUpdateDocuments(String collectionName,
                                                         Bson filter,
                                                         Bson update){
        MongoCollection<Document> collection = getMongoCollection(collectionName);
        return collection.updateMany(filter,update);
    }

    /*public static void main(String[] args) throws IOException {

        MongoDatabase database = new MongoDBConnector().connect();
        MongoCollection<Document> documentCollection = database.getCollection("grades");
        logger.info(documentCollection.getNamespace());
        logger.info(documentCollection.count());
        logger.info(documentCollection.find().first().toJson());
        *//*db.grades.find({
                student_id: {
            $gt: 100
        }
        })*//*

        // korzystanie z MongoDB API

        // zbiór dokumentów spełniających warunki podane w bsonie

        Document document = new Document().append("student_id",new Document("$gt",100));
        Document document2 = new Document("student_id",new Document("$gt",100)).append("type","exam");
        logger.info("nasz bson: "+document2.toJson());
        logger.info(documentCollection.find(document2));
        //logger.info(documentCollection.find(gt("student_id",100)));

        FindIterable<Document> documents = documentCollection.find(document);

        JsonWriterSettings.Builder jsonWriterSettings = JsonWriterSettings.builder().indent(true);
        //MongoCursor<Document> iterator = documents.iterator();
//        while(iterator.hasNext()){
//            logger.info("Kolejny: "+iterator.next().toJson(jsonWriterSettings.build()));
//        }
//        iterator.close();

        FindIterable<Document> documents1 = documentCollection.find(document2)
                .projection(new Document().append("_id",0).append("student_id",1).append("type",1));
        MongoCursor<Document> iterator1 = documents1.iterator();
        while(iterator1.hasNext()){
            System.out.println(iterator1.next().toJson(jsonWriterSettings.build()));
        }

    }*/

    /**
     * 1. Dodać logowanie i pisać junity
     * 2. Dodać kolekcję do bazy danych
     * 3. Wyświetlić liczbę dokumentów w kolekcji
     * 4. Identyfikatory studentów większe od 100
     * 5. Oceny studentów większe o id > 100 z typem oceny = exam
     * 6. Wyświetl identyfikatory studentów ze średnią
     */
}
