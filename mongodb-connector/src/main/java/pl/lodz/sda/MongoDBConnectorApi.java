package pl.lodz.sda;

import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.List;

public interface MongoDBConnectorApi {
    FindIterable<Document> findDocuments(String collectionName,
                                         Bson filter,
                                         Bson projection);
    AggregateIterable<Document> findAndAggregate(String collectionName,
                                                 List<Bson> pipeline);
    UpdateResult findAndUpdateDocuments(String collectionName,
                                        Bson filter,
                                        Bson update);
}
