package julia.codes;


import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.MapJobRepositoryFactoryBean;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.concurrent.Executor;

//TODO
//Reader Listener
//Writer Listener
//JobParametersValidator
//JobLauncher https://gist.github.com/benas/9355801 umożliwi nam zaplanowanie jobów
//https://docs.spring.io/spring-batch/trunk/reference/html/configureJob.html#advancedMetaData
@Configuration
@EnableBatchProcessing // umożliwiamy batchowe procesowanie
@EnableAsync
@EnableScheduling
@PropertySource(value = {"file:${user.home}/myconfiguration.properties", "classpath:/cron.properties"}, ignoreResourceNotFound = true)
public class BatchConfiguration {

    // wstrzykujemy do writera, którego używamy do zapisywania do bazy
    @Qualifier("dataSource")
    @Autowired
    DataSource dataSource;

    @Autowired
    JobBuilderFactory jobBuilderFactory;

    @Autowired
    StepBuilderFactory stepBuilderFactory;

    @Autowired
    JednorozecItemReadListener jednorozecItemReadListener;

    @Autowired
    JednorozecItemWriteListener jednorozecItemWriteListener;

    @Value("${savejednorozec.job.cron}")
    String saveJednorozecCronConfig;

    @Value("${myconf}")
    String juliaConfig;

    @Bean
    JdbcBatchItemWriter<Jednorozec> jednorozecWriter() {
        JdbcBatchItemWriter<Jednorozec> writer = new JdbcBatchItemWriter();
        writer.setDataSource(dataSource);
        writer.setItemSqlParameterSourceProvider(
                new BeanPropertyItemSqlParameterSourceProvider<Jednorozec>());
        writer.setSql("INSERT INTO jednorozec (id,imie,nazwisko,email,plec) " +
                "VALUES (:id," +
                ":imie," +
                ":nazwisko," +
                ":email," +
                ":plec)");
        writer.afterPropertiesSet();
        return writer;
    }

    @Bean
    FlatFileItemReader<Jednorozec> jednorozecReader() {
        FlatFileItemReader<Jednorozec> flatFileItemReader = new FlatFileItemReader<Jednorozec>();
        flatFileItemReader.setResource(new ClassPathResource("jednorozec.csv"));
        flatFileItemReader.setLinesToSkip(1);

        DelimitedLineTokenizer delimitedLineTokenizer = new DelimitedLineTokenizer();
        delimitedLineTokenizer.setNames(new String[]{
                "id",
                "imie",
                "nazwisko",
                "email",
                "plec"});

        BeanWrapperFieldSetMapper<Jednorozec> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(Jednorozec.class);

        DefaultLineMapper<Jednorozec> mapper = new DefaultLineMapper<>();
        mapper.setLineTokenizer(delimitedLineTokenizer);
        mapper.setFieldSetMapper(fieldSetMapper);
        flatFileItemReader.setLineMapper(mapper);


        return flatFileItemReader;
    }

    @Bean
    ItemProcessor<Jednorozec, Jednorozec> processor() {
        return new JednorozecItemProcessor();
    }

    @Bean
    Job saveJednorozec(JednorozecJobListener listener) {
        return jobBuilderFactory.get("nameOfTheJob").
                listener(listener).
                flow(firstMigrationStep()).
                end().
                build();
    }

    @Bean
    Step firstMigrationStep() {
        return stepBuilderFactory.
                get("nameOfTheStep").
                <Jednorozec, Jednorozec>
                chunk(100).
                reader(jednorozecReader()).listener(jednorozecItemReadListener).
                processor(processor()).
                writer(jednorozecWriter()).listener(jednorozecItemWriteListener).
                build();
    }

    @Bean("batchThreadPool")
    public Executor createThreadPool() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setMaxPoolSize(10); //faktycznie max 10
        threadPoolTaskExecutor.setCorePoolSize(3); // default 1, praktycznie minimalna, jeśli nie ma potrzeby na więcej to nie uruchamia
        threadPoolTaskExecutor.setThreadNamePrefix("julia");
        threadPoolTaskExecutor.setThreadGroupName("jednorozec");
        threadPoolTaskExecutor.initialize();
        return threadPoolTaskExecutor;
    }

}
