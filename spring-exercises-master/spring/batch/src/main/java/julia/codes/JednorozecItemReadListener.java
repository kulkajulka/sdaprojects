package julia.codes;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.log4j.Logger;
import org.springframework.batch.core.ItemReadListener;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.stereotype.Component;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.trimToEmpty;


@Component
public class JednorozecItemReadListener implements ItemReadListener<Jednorozec>{

    private static final Logger LOGGER = Logger.getLogger(JednorozecItemReadListener.class);

    private static final String[] NOT_ALLOWED_GENDERS = {
            "SheMale"
    };

    EmailValidator emailValidator = EmailValidator.getInstance();

    @Override
    public void beforeRead() {

    }

    @Override
    public void afterRead(Jednorozec item) {
        try {
            assertItem(item);
        } catch (JednorozecException e) {
            LOGGER.error(item);
        }
        //LOGGER.info(item);
    }

    private void assertItem(Jednorozec jednorozec) throws JednorozecException {
        if (isEmpty(jednorozec.getImie()) ||
                isEmpty(jednorozec.getNazwisko()) ||
                !validateEmail(jednorozec.getEmail()) ||
                jednorozec.getId() <= 0 ||
                StringUtils.containsAny(jednorozec.getPlec(),NOT_ALLOWED_GENDERS)) {
            throw new JednorozecException("",jednorozec);
        }
    }

    private boolean validateEmail(String email) {
        return emailValidator.isValid(email);
    }

    @Override
    public void onReadError(Exception ex) {

    }
}
