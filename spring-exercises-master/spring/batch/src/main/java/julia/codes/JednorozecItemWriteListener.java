package julia.codes;

import org.apache.log4j.Logger;
import org.springframework.batch.core.ItemWriteListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class JednorozecItemWriteListener implements ItemWriteListener{
    private static final Logger LOGGER = Logger.getLogger(JednorozecItemWriteListener.class);
    @Override
    public void beforeWrite(List items) {
        LOGGER.info("Będę zapisywać!");
    }

    @Override
    public void afterWrite(List items) {
        //items.forEach(LOGGER::info);
    }

    @Override
    public void onWriteError(Exception exception, List items) {

    }
}
