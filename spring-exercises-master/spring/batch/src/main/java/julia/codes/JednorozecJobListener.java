package julia.codes;

import org.apache.log4j.Logger;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

// klaska która zapewnia nam akcję przed i po jobie,
// instancję tej klasy zapinamy na jobie przy jego budowaniu

@Component
public class JednorozecJobListener extends JobExecutionListenerSupport {

    private static final Logger LOGGER = Logger.getLogger(JednorozecJobListener.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public void afterJob(JobExecution jobExecution) { // jobExecution to reprezentacja naszego joba, który zawiera info o jobie
        LOGGER.info("listener! ");
        if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
            List<Jednorozec> zwierzaki = jdbcTemplate.query("SELECT * FROM jednorozec",
                    (rs, number) ->
                            new Jednorozec(rs.getInt(1),
                                    rs.getString(2),
                                    rs.getString(3),
                                    rs.getString(4),
                                    rs.getString(5)));
            zwierzaki.forEach(System.out::println);
        }
    }

    @Override
    public void beforeJob(JobExecution jobExecution) {
        LOGGER.info("jobId: "+jobExecution.getJobId()+"jobCreateTime: "+jobExecution.getCreateTime()
                +"jobStatus: "+jobExecution.getStatus());
    }
}
