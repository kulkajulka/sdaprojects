package julia.codes;

import org.apache.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class JednorozecScheduledController {

    private static final Logger LOGGER = Logger.getLogger(JednorozecScheduledController.class);

    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    Job job;

    @Autowired
    BatchConfiguration batchConfiguration;

    @Async("batchThreadPool")  // metodę odpalamy na wielu wątkach
    @Scheduled(cron = "${savejednorozec.job.cron}") //fixedRate = 1000)//cron = "* * * * * *") // odpalamy crona co sekunde
    public void run() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        Thread thread = Thread.currentThread();
        LOGGER.info(thread.getId()+":::"+thread.getName());
        LOGGER.info(batchConfiguration.saveJednorozecCronConfig);
        LOGGER.info(batchConfiguration.juliaConfig);
        JobParameters jobParameters = new JobParametersBuilder()
                .addLong("time",System.currentTimeMillis()).toJobParameters();
        jobLauncher.run(job,jobParameters);
    }
}
