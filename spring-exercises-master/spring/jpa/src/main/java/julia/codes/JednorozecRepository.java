package julia.codes;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface JednorozecRepository extends CrudRepository<Jednorozec,Integer> {

    @Cacheable("jednorozce")
    List<Jednorozec> findByImie(String imie);

    @Query("select j from Jednorozec j where j.nazwsiko = ?1")
    List<Jednorozec> doTheMagic(String nazwisko);

}
