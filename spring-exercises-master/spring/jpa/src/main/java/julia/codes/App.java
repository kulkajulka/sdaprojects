package julia.codes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;


@SpringBootApplication
@EnableCaching
public class App implements CommandLineRunner
{
    public static void main( String[] args )
    {
        SpringApplication.run(App.class);
    }

    @Autowired
    JednorozecRepository jednorozecRepository; /* dzięki IoC wstrzykujemy odpowiednią implementację tego interfejsu*/

    @Override
    public void run(String... strings) throws Exception {
        Jednorozec jednorozec = new Jednorozec(
                1,"Lady","Loxley","lloxley@gmail.com","female");
        jednorozecRepository.save(jednorozec);

        System.out.println(jednorozecRepository.findAll());
        System.out.println(jednorozecRepository.findByImie("Lady"));
        System.out.println(jednorozecRepository.doTheMagic("Loxley"));
        System.out.println(jednorozecRepository.doTheMagic("Loxley"));
    }
}
