package julia.codes;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Jednorozec {

    @Id
    private int id;

    private String imie;

    private String nazwsiko;

    private String mail;

    private String plec;

    public Jednorozec(int id, String imie, String nazwsiko, String mail, String plec) {
        this.id = id;
        this.imie = imie;
        this.nazwsiko = nazwsiko;
        this.mail = mail;
        this.plec = plec;
    }

    public Jednorozec() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwsiko() {
        return nazwsiko;
    }

    public void setNazwsiko(String nazwsiko) {
        this.nazwsiko = nazwsiko;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPlec() {
        return plec;
    }

    public void setPlec(String plec) {
        this.plec = plec;
    }

    @Override
    public String toString() {
        return "Jednorozec{" +
                "id=" + id +
                ", imie='" + imie + '\'' +
                ", nazwsiko='" + nazwsiko + '\'' +
                ", mail='" + mail + '\'' +
                ", plec='" + plec + '\'' +
                '}';
    }
}
