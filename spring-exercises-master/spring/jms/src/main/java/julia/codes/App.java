package julia.codes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

@SpringBootApplication
@EnableJms
public class App implements CommandLineRunner
{
    @Autowired
    JmsTemplate jmsTemplate;

    @Bean
    MessageConverter messageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("type");
        return converter;
    }

    public static void main(String[] args )
    {
        SpringApplication.run(App.class,args);
    }

    @Override
    public void run(String... strings) throws Exception {
        Message message = new Message();
        message.setId(1);
        message.setTopic("MyFirstMessage!");
        message.setContent("Blablabla");
        jmsTemplate.convertAndSend("messageBox",message);

        Message sweetMessage = new Message();
        sweetMessage.setId(2);
        sweetMessage.setTopic("MyFirstSweetMessage!");
        sweetMessage.setContent("Blelelele");
        jmsTemplate.convertAndSend("messageBox",sweetMessage);

        Message message1 = new Message();
        message1.setId(3);
        message1.setTopic("MyFirstMessage!");
        message1.setContent("Blablabla");
        jmsTemplate.convertAndSend("messageBox",message1);

        Message sweetMessage1 = new Message();
        sweetMessage1.setId(4);
        sweetMessage1.setTopic("MyFirstSweetMessage!");
        sweetMessage1.setContent("Blelelele");
        jmsTemplate.convertAndSend("messageBox",sweetMessage1);

        Message message2 = new Message();
        message2.setId(5);
        message2.setTopic("MyFirstMessage!");
        message2.setContent("Blablabla");
        jmsTemplate.convertAndSend("messageBox",message2);

        Message sweetMessage2 = new Message();
        sweetMessage2.setId(6);
        sweetMessage2.setTopic("MyFirstSweetMessage!");
        sweetMessage2.setContent("Blelelele");
        jmsTemplate.convertAndSend("messageBox",sweetMessage2);

        jmsTemplate.convertAndSend("stringBox","weirdMessage");
    }
}
