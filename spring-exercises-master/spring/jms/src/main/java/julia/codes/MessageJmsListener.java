package julia.codes;

import org.apache.log4j.Logger;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class MessageJmsListener {

    private static final Logger LOGGER = Logger.getLogger(MessageJmsListener.class);
    @JmsListener(destination = "messageBox")
    public void readMessage(Message message) {
        LOGGER.info("We have it: "+message);

    }
    @JmsListener(destination = "messageBox")
    public void readSweetMessage(Message message) {
        LOGGER.info("Sweet message: "+message);
    }

    @JmsListener(destination = "stringBox")
    public void readString(String string) {
        LOGGER.info("String received: "+string);
    }
}
