package julia.codes;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class LoggableApect {

    @Around("@annotation(Loggable)")
    public void loggableAdvice() {
        System.out.println("Logujemy tu bardzo ważne rzeczy!");
    }

    @Before("@annotation(Loggable)")
    public void loggableBeforeAdvice() {
        System.out.println("Tuz przed");
    }

    @After("@annotation(Loggable)")
    public void loggableAfterAdvice() {
        System.out.println("After!");
    }

}
