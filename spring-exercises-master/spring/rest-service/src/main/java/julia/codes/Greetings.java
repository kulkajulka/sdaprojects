package julia.codes;

public class Greetings {

    private final String content;

    public Greetings(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Greetings greetings = (Greetings) o;

        return content != null ? content.equals(greetings.content) : greetings.content == null;
    }

    @Override
    public int hashCode() {
        return content != null ? content.hashCode() : 0;
    }
}
