package julia.codes;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class GreetingsController {

    private static final String template = "Hello, %s!";

    @RequestMapping("/greetings")
    public Greetings greeting(@RequestParam(value="name",required = false, defaultValue="World") String name) {
        return new Greetings(String.format(template, name));
    }
}
