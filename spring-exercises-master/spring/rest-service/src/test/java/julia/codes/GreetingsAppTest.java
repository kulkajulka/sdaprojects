package julia.codes;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static sun.security.krb5.internal.crypto.Nonce.value;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GreetingsAppTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    public void greetingsTest() throws Exception {
        mockMvc.perform(get("/greetings"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("content").value("Hello, World!"));
    }

    @Test
    public void greetingsJuliatest() throws Exception {
        mockMvc.perform(get("/greetings?name=Julia"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("content").value("Hello, Julia!"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }
}