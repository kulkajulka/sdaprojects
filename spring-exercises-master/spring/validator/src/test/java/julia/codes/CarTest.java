package julia.codes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;

public class CarTest {

    @Test
    public void generateCarJson() throws JsonProcessingException {
        Car car = new Car(
                RandomStringUtils.randomAlphabetic(25),
                (int) (Math.random() * 5 +2),
                RandomStringUtils.randomAlphabetic(11),
                RandomStringUtils.randomAlphabetic(10),
                RandomStringUtils.randomAlphabetic(10));

        ObjectWriter ow =
                new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(car);
        System.out.println(json);
    }
}