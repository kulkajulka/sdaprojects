package julia.codes;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CarManager {
    List<Car> cars = new ArrayList<>();

    public void saveCar(Car car){
        cars.add(car);
    }

    public List<Car> findAll(){
        return cars;
    }

    public Car find (Car car){
        return cars.stream().filter(car1 -> car1.equals(car)).findAny().orElse(null);
    }
}
