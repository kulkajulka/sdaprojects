package julia.codes;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Car {

    @NotNull
    @Size(max = 25)
    private String owner;

    @NotNull
    @Min(2)
    @Max(7)
    private int numberOfDoors;

    @NotNull
    @Size(max = 11)
    private String color;

    @NotNull
    @Size(max = 10)
    private String type;

    @NotNull
    @Size(max = 10)
    private String brand;

    public Car(String owner, int numberOfDoors, String color, String type, String brand) {
        this.owner = owner;
        this.numberOfDoors = numberOfDoors;
        this.color = color;
        this.type = type;
        this.brand = brand;
    }

    public Car(){}

    @Override
    public String toString() {
        return "Car{" +
                "owner='" + owner + '\'' +
                ", numberOfDoors=" + numberOfDoors +
                ", color='" + color + '\'' +
                ", type='" + type + '\'' +
                ", brand='" + brand + '\'' +
                '}';
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public int getNumberOfDoors() {
        return numberOfDoors;
    }

    public void setNumberOfDoors(int numberOfDoors) {
        this.numberOfDoors = numberOfDoors;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
