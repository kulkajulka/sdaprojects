package julia.codes;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class CarController {
    @RequestMapping("/car/random")
    public Car randomCar() {
        return new Car(
                RandomStringUtils.randomAlphabetic(25),
                (int) (Math.random() * 5 +2),
                RandomStringUtils.randomAlphabetic(11),
                RandomStringUtils.randomAlphabetic(10),
                RandomStringUtils.randomAlphabetic(10));
    }

    @RequestMapping("/car/random/invalid")
    @ResponseBody
    public @Valid Car randomInvalidCar() {
        return new Car(
                null,
                (int) (Math.random() * 5 +2),
                RandomStringUtils.randomAlphabetic(11),
                RandomStringUtils.randomAlphabetic(10),
                RandomStringUtils.randomAlphabetic(10));
    }

    @RequestMapping(value = "/validate",method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String validate(@Valid @RequestBody Car car) {
        return "SUCCESS";
    }
}
