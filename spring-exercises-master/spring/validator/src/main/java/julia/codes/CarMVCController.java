package julia.codes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.validation.Valid;
import java.awt.*;

@Controller
public class CarMVCController extends WebMvcConfigurerAdapter {

    @Autowired
    CarManager carManager;

    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("").setViewName("");
    }

    @PostMapping(value = "/")
    public String submitForm(@Valid Car car, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) return "form";
        carManager.saveCar(car);
        return "result";
    }

    @GetMapping("/")
    public String showForm(Car car) {
        return "form";
    }

    @GetMapping("/cars")
    public String showCars(Model model) {
        model.addAttribute("cars",carManager.findAll());
        return "cars";
    }
}
