package julia.codes;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Scheduler {
    private static final Logger LOGGER = Logger.getLogger(Scheduler.class);

    @Scheduled(fixedDelay = 1000/* cron = "* * * * * *" też do ustawienia częstotliwości*/)
    public void log() {
        LOGGER.info("Hello!");
    }
}
