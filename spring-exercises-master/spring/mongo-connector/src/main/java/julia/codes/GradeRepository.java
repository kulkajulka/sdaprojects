package julia.codes;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface GradeRepository extends MongoRepository<Grade,Integer> {
    Grade findByStudentId(int student_id);
}
