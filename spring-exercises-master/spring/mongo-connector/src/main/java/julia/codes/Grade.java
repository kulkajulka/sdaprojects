package julia.codes;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.bson.types.ObjectId;

import java.io.Serializable;

public class Grade
{
    private ObjectId _id;
    private int studentId;
    private String type;
    private double score;

    public Grade() {
    }

    public Grade(ObjectId _id, int studentId, String type, double score) {
        super();
        this._id = _id;
        this.studentId = studentId;
        this.type = type;
        this.score = score;
    }

    public ObjectId getId() {
        return _id;
    }

    public void setId(ObjectId _id) {
        this._id = _id;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Grade grade = (Grade) o;

        if (studentId != grade.studentId) return false;
        if (Double.compare(grade.score, score) != 0) return false;
        if (!_id.equals(grade._id)) return false;
        return type.equals(grade.type);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = _id.hashCode();
        result = 31 * result + studentId;
        result = 31 * result + type.hashCode();
        temp = Double.doubleToLongBits(score);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "id=" + _id +
                ", studentId=" + studentId +
                ", type='" + type + '\'' +
                ", score=" + score +
                '}';
    }
}
