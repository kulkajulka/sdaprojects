package julia.codes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.core.MongoTemplate;

@SpringBootApplication
public class App implements CommandLineRunner
{

    @Autowired
    UserRepository userRepository;

    @Autowired
    GradeRepository gradeRepository;

    @Autowired
    MongoTemplate mongoTemplate;

    public static void main( String[] args )
    {
        SpringApplication.run(App.class);
    }

    @Override
    public void run(String... strings) throws Exception {
        //userRepository.deleteAll();
        //User user = new User(3,"Andrzej","Piaseczny",
          //      "********");
        //userRepository.insert(user);
        System.out.println(mongoTemplate.getCollectionNames());
        System.out.println(mongoTemplate.findAll(User.class,"user"));
        System.out.println(mongoTemplate.findAll(Grade.class,"grades"));
        System.out.println(userRepository.findOne(1));
    }
}
