package julia.codes;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ChuckNorrisCollectorTest {

    @Autowired
    ChuckNorrisCollector chuckNorrisCollector;

        @Test
     public void getRandomJoke() throws Exception {
        CompletableFuture<ChuckNorrisJoke> chuckNorrisJoke = chuckNorrisCollector.getRandomJoke();
        System.out.println(chuckNorrisJoke);
  }

}