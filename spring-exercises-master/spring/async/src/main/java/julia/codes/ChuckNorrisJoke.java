package julia.codes;

import java.util.List;

public class ChuckNorrisJoke {

    private String type;

    private JokeContent value;

    public ChuckNorrisJoke() {
    }

    public ChuckNorrisJoke(String type, JokeContent value) {
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public JokeContent getValue() {
        return value;
    }

    public void setValue(JokeContent value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ChuckNorrisJoke{" +
                "type='" + type + '\'' +
                ", value=" + value +
                '}';
    }
}
