package julia.codes;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

@Service
public class ChuckNorrisCollector {

    RestTemplate restTemplate = new RestTemplate();

    CompletableFuture<ChuckNorrisJoke> getRandomJoke() {
        return CompletableFuture.completedFuture(restTemplate.
                getForObject("http://api.icndb.com/jokes/random",ChuckNorrisJoke.class));
    }

    //@Async("chuckNorrisThreadPool")
    public void invokeGetRandomJoke() throws InterruptedException {
        System.out.println("begin "+Thread.currentThread().getName());
        System.out.println(getRandomJoke());
        System.out.println("end "+Thread.currentThread().getName());
        Thread.sleep(1000);
    }
}
