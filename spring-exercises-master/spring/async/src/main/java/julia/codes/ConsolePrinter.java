package julia.codes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.Executor;

@Component
@EnableAsync
public class ConsolePrinter implements CommandLineRunner {

    @Autowired
    ChuckNorrisCollector chuckNorrisCollector;

    @Override
    public void run(String... strings) throws Exception {

    }

    @Scheduled(fixedRate = 1000)
    public void printJoke(){
        System.out.println("beginIn: "+Thread.currentThread().getName());
        try {
            chuckNorrisCollector.invokeGetRandomJoke();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("endIn: "+Thread.currentThread().getName());
    }

    @Bean("chuckNorrisThreadPool")
    public Executor createThreadPool() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setMaxPoolSize(10); //faktycznie max 10
        threadPoolTaskExecutor.setCorePoolSize(2); // default 1, praktycznie minimalna, jeśli nie ma potrzeby na więcej to nie uruchamia
        threadPoolTaskExecutor.setThreadNamePrefix("julia");
        threadPoolTaskExecutor.setThreadGroupName("norris");
        threadPoolTaskExecutor.initialize();
        return threadPoolTaskExecutor;
    }
}
