package julia.codes;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class HelloWorld {
    private String hello;

    public HelloWorld(String hello) {
        this.hello = hello;
    }

    public HelloWorld(){}

    @Override
    public String toString() {
        return "HelloWorld("+hello+")";
    }

    @PostConstruct
    private void setHello(){
        hello = "hello!";
    }
}
