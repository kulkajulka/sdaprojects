package julia.codes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class App implements CommandLineRunner
{
    @Autowired
    ApplicationContext context;

    @Autowired
    HelloWorld helloWorld;

    public static void main( String[] args )
    {

        SpringApplication.run(App.class,args);
    }

    public void run(String... strings) throws Exception {
        System.out.println("test wyświetlania");
        System.out.println(context.getBeanDefinitionNames());
        System.out.println(helloWorld);
    }
}
