package julia.codes;

import org.junit.Test;

import javax.validation.*;

import java.util.Set;

import static org.junit.Assert.*;

public class ManagerTest {

    @Test
    public void validationTest() {
        Manager invalidmanager = new Manager(
                "jan","Nowak",-9, "");
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        Set<ConstraintViolation<Manager>> constraints = validator.validate(invalidmanager);
        constraints.forEach(System.out::println);
    }
}