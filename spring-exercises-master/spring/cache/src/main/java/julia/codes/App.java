package julia.codes;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import javax.swing.*;

@SpringBootApplication
@EnableCaching
public class App implements CommandLineRunner
{
    private static final Logger log = Logger.getLogger(App.class);
    public static void main( String[] args )
    {
        SpringApplication.run(App.class);
    }

    @Autowired
    ManagerRepository managerRepository;
    
    @Override
    public void run(String... strings) throws Exception {
        log.info("first invocstion");
        log.info("manager: "+managerRepository.getManager());
        log.info("manager cache: "+ managerRepository.getCacheManager());
        log.info("second invocation");
        log.info("manager: "+ managerRepository.getManager());
        log.info("cache manager: "+ managerRepository.getCacheManager());
        log.info("third invocation");
        log.info("manager: "+ managerRepository.getManager());
        log.info("cache manager: "+ managerRepository.getCacheManager());
    }
}
