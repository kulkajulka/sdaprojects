package julia.codes;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component
public class ManagerRepository {
    public Manager getManager() {
        sleep();
        return new Manager("Krzysztof","Jarzyna",32,"Kompany");
    }

    @Cacheable("managers")
    public Manager getCacheManager() {
        sleep();
        return new Manager("Krzysztof","jarzyna",23,"Firma");
    }

    private void sleep(){
        try {
            Thread.sleep(4000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
