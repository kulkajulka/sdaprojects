package julia.codes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootApplication
public class App implements CommandLineRunner{
    /* implementowanie tego insterfejsu umożliwia wyślwietlanie w konsoli */
    public static void main(String[] args) {
        SpringApplication.run(App.class);
    }

    @Autowired
    JdbcTemplate jdbcTemplate;

    public void run(String... strings) throws Exception {
        jdbcTemplate.execute("DROP TABLE if EXISTS jednorozec");
        jdbcTemplate.execute("create table jednorozec (" +
                "    id INT," +
                "    imie VARCHAR(50)," +
                "    nazwisko VARCHAR(50)," +
                "    email VARCHAR(50)," +
                "    plec VARCHAR(50))");
        jdbcTemplate.execute("insert into jednorozec" +
                " (id, imie, nazwisko, email, plec) values " +
                "(1, 'Leslie', 'Clulow', 'lclulow0@cbslocal.com', 'Male')");

        jdbcTemplate.execute("insert into jednorozec" +
                " (id, imie, nazwisko, email, plec) values " +
                "(2, 'Lady', 'Loxley', 'lloxley0@cbslocal.com', 'Female')");

        jdbcTemplate.batchUpdate("insert into jednorozec (id, imie, nazwisko, email, plec) values (2, 'Judon', 'Romanini', 'jromanini1@sun.com', 'Male');\n" +
                "insert into jednorozec (id, imie, nazwisko, email, plec) values (3, 'Aurilia', 'Kersting', 'akersting2@google.fr', 'Female');\n" +
                "insert into jednorozec (id, imie, nazwisko, email, plec) values (4, 'Ulla', 'Lamden', 'ulamden3@fotki.com', 'Female');\n" +
                "insert into jednorozec (id, imie, nazwisko, email, plec) values (5, 'Jania', 'Littell', 'jlittell4@homestead.com', 'Female');\n" +
                "insert into jednorozec (id, imie, nazwisko, email, plec) values (6, 'Beitris', 'Halligan', 'bhalligan5@reverbnation.com', 'Female');\n" +
                "insert into jednorozec (id, imie, nazwisko, email, plec) values (7, 'Jaquenette', 'Humphris', 'jhumphris6@amazon.de', 'Female');\n" +
                "insert into jednorozec (id, imie, nazwisko, email, plec) values (8, 'Susette', 'Wescott', 'swescott7@biblegateway.com', 'Female');\n" +
                "insert into jednorozec (id, imie, nazwisko, email, plec) values (9, 'Cecil', 'Esome', 'cesome8@ezinearticles.com', 'Male');\n" +
                "insert into jednorozec (id, imie, nazwisko, email, plec) values (10, 'Merwyn', 'Marrion', 'mmarrion9@omniture.com', 'Male')");


        System.out.println(changeSex("shemale",1));

        List<Jednorozec> stajnia = jdbcTemplate.query("select * from jednorozec",
                (rs, number) -> new Jednorozec(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5)));
        System.out.println(stajnia);

        transactionalChangeSex("shemale",1);

        List<Jednorozec> stajnia2 = jdbcTemplate.query("select * from jednorozec",
                (rs, number) -> new Jednorozec(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5)));
        System.out.println(stajnia2);
    }

    private int changeSex(String newSex, int unicornId) {
        return jdbcTemplate.update("update jednorozec set plec = ? where id = ?",newSex,unicornId);

    }

    @Transactional /* nie działa raczej */
    void transactionalChangeSex(String newSex, int unicornId) {
        jdbcTemplate.update("update jednorozec set plec = ? where id = ?",newSex,unicornId);
        throw new RuntimeException();
    }
}
