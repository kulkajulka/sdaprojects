package pl.lodz.sda.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.lodz.sda.model.Company;
import pl.lodz.sda.service.CompanyService;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class CompanyController {

    CompanyService companyService = new CompanyService();

    @RequestMapping("/company/first")
    public Company getFirstCompany(){
        return companyService.findFirstCompany();
    }

    @RequestMapping("/company")
    public Company getCompany(@RequestParam("id") String id){
        return companyService.findCompany(Long.valueOf(id));
    }

    @RequestMapping(value = "/companies",
                    produces = "application/json")
    public List<Company> getAllCompanies(){
        System.out.println("Szukam companies!");
        return companyService.findAllCompanies();
    }

    @RequestMapping(value = "/company/save",method = POST,
                    consumes = "application/json",
                    produces = "application/json")
    public Company save(@RequestBody Company company){
        Company testComp = companyService
                .saveCompany(company);
        if (testComp.getId() != null) return testComp;
        else return null;
    }
    @RequestMapping(value = "/company/savemany",method = POST,
            consumes = "application/json",
            produces = "application/json")
    public List<Company> saveMany(@RequestBody List<Company> companies){
        companies.forEach(companyService::saveCompany);
        return companies;
    }

}
