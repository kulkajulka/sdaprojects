package pl.lodz.sda.tools;

import pl.lodz.sda.environment.DB;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader {
    public static String PATH = "connection.properties";
    Properties prop = new Properties();

    public void init() throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(PATH);

        if (inputStream != null) {
            try {
                prop.load(inputStream);
            } catch (IOException e) {
            } finally {
                inputStream.close();
            }
        } else {
            throw new FileNotFoundException("property file '" + PATH + "' not found in the classpath");
        }
    }
    public String getProperty(String key) {
        return prop.getProperty(key);
    }

    public DB getDB(){
        return DB.valueOf(getProperty("db.engine"));
    }
}
