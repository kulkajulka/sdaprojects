package pl.lodz.sda.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "ID")
//@DiscriminatorValue("contractemployee")
public class ContractEmployee extends Employee{
    @Column(name = "hour_rate")
    private int hourRate;

    public ContractEmployee(long id, String name, int hourRate) {
        super(id, name);
        this.hourRate = hourRate;
    }

    public ContractEmployee(int hourRate) {
        this.hourRate = hourRate;
    }
    public ContractEmployee(){

    }

    public int getHourRate() {
        return hourRate;
    }

    public void setHourRate(int hourRate) {
        this.hourRate = hourRate;
    }

    @Override
    public String toString() {
        return "ContractEmployee{" +
                "hourRate=" + hourRate +
                ", id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
