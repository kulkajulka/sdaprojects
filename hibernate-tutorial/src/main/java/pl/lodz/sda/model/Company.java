package pl.lodz.sda.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "company")
public class Company implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "company_id")
    Long company_id;

    @Column(name = "name")
    String name;

    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    Set<Department> department = new HashSet<>();

    @OneToOne(
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    @JoinColumn(name = "companyAddress")
    Address address;

    public Company() {
    }

    public Company(String name, Set<Department> department,Address address) {
        this.name = name;
        this.department = department;
        this.address = address;
    }

    public Long getId() {
        return company_id;
    }

    public void setId(Long id) {
        this.company_id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Department> getDepartment() {
        return department;
    }

    public void setDepartment(Set<Department> department) {
        this.department = department;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + company_id +
                ", name='" + name + '\'' +
                ", departmentsSize=" + department.size() +
                ", address="+address+
                '}';
    }
}
