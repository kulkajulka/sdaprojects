package pl.lodz.sda.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "ID")
//@DiscriminatorValue("regularemployee")
public class RegularEmployee extends Employee{
    @Column
    int salary;

    public RegularEmployee(long id, String name, int salary) {
        super(id, name);
        this.salary = salary;
    }

    public RegularEmployee(int salary) {
        this.salary = salary;
    }

    public RegularEmployee(){}

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "RegularEmployee{" +
                "salary=" + salary +
                ", id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
