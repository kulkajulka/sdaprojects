package pl.lodz.sda.model;

import javax.persistence.Embeddable;

@Embeddable
public class DepartmentAddress {
    private String street;
    private String city;
    private String country;

    public DepartmentAddress(String street, String city, String country) {
        this.street = street;
        this.city = city;
        this.country = country;
    }

    public DepartmentAddress(){

    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
