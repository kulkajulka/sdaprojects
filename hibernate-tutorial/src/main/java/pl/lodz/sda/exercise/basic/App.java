package pl.lodz.sda.exercise.basic;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.lodz.sda.model.Task;
import pl.lodz.sda.environment.DB;
import pl.lodz.sda.tools.HibernateSessionFactory;

/**
 *
 */
public class App {
    public static void main(String[] args) {

        // Tworzymy sesję
        System.out.println("create session");
        HibernateSessionFactory hibernateSessionFactory =
                new HibernateSessionFactory(DB.H2);
        Session session = HibernateSessionFactory.createSession();
        try {
            // Rozpoczynamy transakcję w sesji
            System.out.println("Begin transaction");
            Transaction tx = session.beginTransaction();
            Task task = new Task();
            Task task2 = new Task("Hello!","Hello desc");
            task.setName("Hello world task");
            task.setDescription("Hello world task description");

            for (int i = 0; i < 1000000; i++) {
                Task newTask = new Task("task"+i,"desc"+i);
                session.save(newTask);
                if (i%50==0){
                    session.flush();
                    session.clear();
                }
            }


            // zapisujemy w sesji nasz obiekt
            System.out.println("save");  // odpowiednik execute w JDBC
            session.save(task);
            session.save(task2);
            // Commitujemy zmiany
            System.out.println("commit");
            System.out.println(session.getIdentifier(task));
            System.out.println(session.getIdentifier(task2));
            System.out.println(session.getStatistics());
            tx.commit();
        } catch (Exception e) {
            //log
        } finally {
            // Zamykamy sessionFactory
            HibernateSessionFactory.closeAll(session);
        }

    }
}
