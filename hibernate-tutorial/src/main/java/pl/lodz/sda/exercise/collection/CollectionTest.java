package pl.lodz.sda.exercise.collection;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import pl.lodz.sda.model.Address;
import pl.lodz.sda.model.Company;
import pl.lodz.sda.model.Department;
import pl.lodz.sda.model.DepartmentAddress;
import pl.lodz.sda.environment.DB;
import pl.lodz.sda.tools.HibernateSessionFactory;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CollectionTest {

    public static void main(String[] args) {

        Company company = new Company();
        company.setName("test");
        Company company1 = new Company();
        company1.setName("test2");
        Company company2 = new Company();
        company2.setName("test3");

        DepartmentAddress departmentAddress = new DepartmentAddress();
        departmentAddress.setCity("Łódź");
        departmentAddress.setCountry("Polska");
        departmentAddress.setStreet("Wólczańska");

        Department dp = new Department(company);
        dp.setDepartmentAddress(departmentAddress);
        Department dp2 = new Department(company);
        Department dp3 = new Department(company);
        Department dp4 = new Department(company);
        Department dp5 = new Department(company);
        Department dp6 = new Department(company);
        Set<Department> dps = new HashSet<>();
        Set<Department> dps1 = new HashSet<>();
        Set<Department> dps2 = new HashSet<>();
        dps.add(dp);
        dps.add(dp2);
        dps1.add(dp3);
        dps1.add(dp4);
        dps2.add(dp5);
        dps2.add(dp6);
        company.setDepartment(dps);
        company1.setDepartment(dps1);
        company2.setDepartment(dps2);
        Address address = new Address();
        Address address1 = new Address();
        Address address2 = new Address();
        address.setCountry("Poland");
        address1.setCountry("Germany");
        address2.setCountry("England");
        company.setAddress(address);
        company1.setAddress(address1);
        company2.setAddress(address2);

        Session session = null;
        Transaction tx;
        List<Company> list;
        HibernateSessionFactory hibernateSessionFactory =
                new HibernateSessionFactory(DB.H2);
        try {
            session = HibernateSessionFactory.createSession();

            tx = session.beginTransaction();
            session.save(company);
            session.save(company1);
            session.save(company2);
            tx.commit();

          /*  tx = session.beginTransaction();
            session.save(dp);
            session.save(dp2);
            tx.commit();
*/
            tx = session.beginTransaction();
            String sql = "SELECT * FROM company c";
            SQLQuery query = session.createSQLQuery(sql);
            query.addEntity(Company.class);
            list = query.list();
            list.forEach(System.out::println);
            //session.delete(company);
            Criteria cr = session.createCriteria(Company.class);
            cr.add(Restrictions.or(Restrictions.eq("name","test"),
                    Restrictions.eq("name","test2")));
            cr.list().forEach(System.out::println);

            Criteria criteria = session.createCriteria(Department.class);
            criteria.list().forEach(System.out::println);
            tx.commit();

            /*System.out.println(company);
            System.out.println(dp);
            System.out.println(dp2);
            System.out.println(address);*/
        } catch (Exception e) {
            System.out.println("Exception occured. " + e.getMessage());
            e.printStackTrace();
        } finally {
            HibernateSessionFactory.closeAll(session);
        }


    }
}
