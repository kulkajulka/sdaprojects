package pl.lodz.sda.exercise.basic;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.lodz.sda.environment.DB;
import pl.lodz.sda.tools.HibernateSessionFactory;

public class AppMysql{
        public static void main(String[] args) {

            // Tworzymy sesję
            System.out.println("create session");
            Session session = null;
            HibernateSessionFactory hibernateSessionFactory =
                    new HibernateSessionFactory(DB.H2);
            try {
                session = HibernateSessionFactory.createSession();
                System.out.println("Begin transaction");
                Transaction tx = session.beginTransaction();
                tx.commit();
            } catch (Exception e) {
                //log
            } finally {
                // Zamykamy sessionFactory
                HibernateSessionFactory.closeAll(session);
            }

        }
}
