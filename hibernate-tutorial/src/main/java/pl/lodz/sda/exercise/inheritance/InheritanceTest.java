package pl.lodz.sda.exercise.inheritance;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import pl.lodz.sda.model.ContractEmployee;
import pl.lodz.sda.model.Employee;
import pl.lodz.sda.model.RegularEmployee;
import pl.lodz.sda.environment.DB;
import pl.lodz.sda.tools.HibernateSessionFactory;

public class InheritanceTest {
    public static void main(String[] args) {
        ContractEmployee contractEmployee = new ContractEmployee();
        contractEmployee.setName("Janek Kowalski");
        contractEmployee.setHourRate(13);
        RegularEmployee regularEmployee = new RegularEmployee();
        regularEmployee.setSalary(15000);
        regularEmployee.setName("Julia Wojna");

        HibernateSessionFactory hibernateSessionFactory =
                new HibernateSessionFactory(DB.H2);
        Session session = null;
        try {
            session = HibernateSessionFactory.createSession();
            Transaction transaction = session.beginTransaction();
            session.save(contractEmployee);
            session.save(regularEmployee);
            transaction.commit();

            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(Employee.class);
            criteria.add(Restrictions.isNotNull("salary"));
            criteria.list().forEach(System.out::println);
            transaction.commit();

            SQLQuery sqlQuery = session.createSQLQuery("SELECT * FROM ContractEmployee");
            sqlQuery.addEntity(ContractEmployee.class);
            sqlQuery.list().forEach(System.out::println);

        } catch (Exception e) {
            System.out.println("Exception occured. " + e.getMessage());
            e.printStackTrace();
        } finally {
            HibernateSessionFactory.closeAll(session);
        }

    }
}
