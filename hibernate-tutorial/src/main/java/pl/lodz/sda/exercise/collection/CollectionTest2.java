package pl.lodz.sda.exercise.collection;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.lodz.sda.model.Address;
import pl.lodz.sda.model.Company;
import pl.lodz.sda.model.Department;
import pl.lodz.sda.model.DepartmentAddress;
import pl.lodz.sda.environment.DB;
import pl.lodz.sda.tools.HibernateSessionFactory;

import java.util.HashSet;
import java.util.Set;

public class CollectionTest2 {
    public static void main(String[] args) {
        Transaction tx;
        Session session = null;
        SessionFactory sessionFactory = null;

        DepartmentAddress departmentAdress = new DepartmentAddress("street", "city", "country");

        Address address = new Address("Piotrkowska",
                101,
                "93-333",
                "Poland",
                null);

        Company company = new Company();
        company.setName("test");
        company.setAddress(address);

        Department dp = new Department(company);
        dp.setDepartmentAddress(departmentAdress);
        Department dp2 = new Department(company);
        dp2.setDepartmentAddress(departmentAdress);
        Set<Department> dps = new HashSet<>();
        dps.add(dp);
        dps.add(dp2);
        company.setDepartment(dps);
        address.setCompany(company);

        try{
            sessionFactory = HibernateSessionFactory.createSessionFactory(DB.H2);
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            session.save(company);
            System.out.println(company);
            tx.commit();

            session = sessionFactory.openSession();
            session.beginTransaction();
            Company company1 = session.get(Company.class, 1l);
            company1.getDepartment();
            session.getTransaction().commit();
            session.close();

            //System.out.println(company1.getDepartment()); jeśli FetchType.LAZY to nie uda się
            // ta akcja, bo obiekt po zamknieciu sesji przepada 
        } catch (Exception e) {
            System.out.println("Exception occured. " + e.getMessage());
            e.printStackTrace();
        } finally {
            HibernateSessionFactory.closeSessionFactory();
        }
    }
}
