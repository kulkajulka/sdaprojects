package pl.lodz.sda.service;

import org.hibernate.Session;
import pl.lodz.sda.dao.CompanyManager;
import pl.lodz.sda.model.Company;
import pl.lodz.sda.tools.HibernateSessionFactory;

import java.io.IOException;
import java.util.List;

public class CompanyService {
    private CompanyManager companyManager;

    public CompanyService() {
        this.companyManager = new CompanyManager();
    }

    public Company saveCompany(Company company){
        return companyManager.save(company);
    }

    public Company findFirstCompany(){
        return companyManager.find(1L);
    }

    public List<Company> findAllCompanies(){
        return companyManager.findAll();
    }

    public Company findCompany(Long id){
        return companyManager.find(id);
    }
}