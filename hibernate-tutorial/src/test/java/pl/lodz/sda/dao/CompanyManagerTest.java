package pl.lodz.sda.dao;

import org.junit.*;
import pl.lodz.sda.model.Company;
import pl.lodz.sda.tools.HibernateSessionFactory;

import java.io.IOException;

import static com.sun.javaws.JnlpxArgs.verify;
import static org.junit.Assert.assertEquals;

public class CompanyManagerTest {

    private CompanyManager companyManager;
    @Before
    public void setUp(){
        companyManager = new CompanyManager();
    }
    @AfterClass
    public void closeDBConnection(){

    }

    @Test
    public void saveTest() throws Exception {
        Company company = new Company();
        Assert.assertEquals(company,companyManager.save(company));
    }

    @Test
    public void update() throws Exception {
    }

    @Test
    public void find() throws Exception {
        Company company = new Company();
        company.setId(2L);
        companyManager.save(company);
        Assert.assertEquals(company.getId(),companyManager.find(company.getId()).getId());
    }

    @Test
    public void findAll() throws Exception {
    }

    @Test
    public void deleteAll() throws Exception {
    }

    @Test
    public void delete() throws Exception {
    }

}