package julia.codes;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/") // informavcja o tym na pod jakim adresem znajduje się nasz servlet
public class HelloWorldServlet extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Wpadło żądanie na ten adres!");
        PrintWriter out = resp.getWriter(); // słuzy do wyrzucenia zawartości, którą użytkownika wyświetli
        out.print("<h1 style=\"text-align: center\">Hello World!</h1>");
        out.print("na adresie "+req.getRequestURI()); // URI jest bez adresu hosta, URL z
        req.getQueryString();
    }

    @Override
    public void destroy() {
        super.destroy();
        System.out.println("Niszczymy servlet!");
    }

    @Override
    public void init() throws ServletException {
        super.init();
        System.out.println("Inicjujemy servlet!");
    }
}
