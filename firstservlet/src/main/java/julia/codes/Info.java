package julia.codes;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Enumeration;
import java.util.Map;
import java.util.Set;

@WebServlet("/info")
public class Info extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String logRequestParam = getInitParameter("LOG-REQUEST");
        String applicationName = getInitParameter("APPLICATION-NAME");
        if ("true".equalsIgnoreCase(logRequestParam)) {
            System.out.println("Wpadło żądanie na ten adres!");

        }

        PrintWriter out = resp.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<head>\n" +
                "<title>"+applicationName+" </title>\n" +
                "</head>");
        out.println("<html>");
        out.println("<body>");
        out.println("<h3 >"+applicationName+"</h3>");
        out.println("<p > Adres URL "+req.getRequestURI()+"</p>");
        out.println("<p > Dodatkowe parametry: "+req
        .getQueryString()+"</p>");
        out.println("<p>Czas: "+ LocalDateTime.now()+"</p>");
        HttpSession session = req.getSession();
        out.println("<p> Session parameters: </p>");
        Enumeration<String> attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String next = attributeNames.nextElement();
            out.println("<p> Next attribute: "+next+", "+
                    session.getAttribute(next)+"</p>");
        }
        out.println("<p> Tu ciastka: </p>");
        Cookie[] cookies = req.getCookies();
        out.println(cookies.length);
        for (Cookie cookie : cookies) {
            out.println("<p> Ciasteczko: "+cookie.getName()+", "+cookie.getValue()+"</p>");
        }

        if (req.getRequestURI().equals("/firstservlet/additional-info")) {
            out.println("<p> IP :"+req.getRemoteAddr()+"</p>");
            Map<String, ? extends ServletRegistration> servletRegistrations = req.getServletContext().getServletRegistrations();
            servletRegistrations.entrySet().stream().forEach(entry ->
                    out.println("<p>Key: "+entry.getKey()+", Value: "+entry.getValue().getMappings()+"</p>"));
        }

        out.println("</body>");
        out.println("</html>");
        session.setAttribute("Data ostatniego zadania: ",LocalTime.now());
        session.setAttribute("adres Url",req.getRequestURL());
        resp.addCookie(new Cookie("ciastko","uzytkownik"));
        resp.addCookie(new Cookie("data",LocalTime.now().toString()));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }

    @Override
    public void destroy() {
        super.destroy();
    }


    @Override
    public void init() throws ServletException {
        super.init();
    }
}
