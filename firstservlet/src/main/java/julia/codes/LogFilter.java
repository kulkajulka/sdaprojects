package julia.codes;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebFilter(servletNames = {"Info"})
public class LogFilter implements Filter{
    private static final Logger LOGGER = Logger.getLogger(LogFilter.class.getName());
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOGGER.log(Level.INFO,"utworzenie instancji filtra");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            String url = httpRequest.getRequestURL().toString();
            String queryParams = httpRequest.getQueryString();
            LOGGER.log(Level.INFO,String.format("Wywołano adres URL %s z parametrami: %s",url,queryParams));
        }
        chain.doFilter(request,response);
    }

    @Override
    public void destroy() {

    }
}
